// EbmServer.h
//
#ifndef EBMSERVER_H
#define EBMSERVER_H

#include "Poco/Net/DatagramSocket.h"
#include "Poco/Net/SocketReactor.h"
#include "Poco/Net/SocketNotification.h"
#include "Poco/Thread.h"
#include "../client/NhdpProtoClient.h"
//#include "../client/NhdpProtoClientNew.h"
#include "../algo/ECDS.h"
#include "../algo/SMPR.h"
#include "../client/EbmProtoClient.h"

/*struct EbmMessage {
    int priority;
    std::string id;
    std::string address;
    bool visited;
};*/

class EbmServer
{
public:
    EbmServer();
    EbmServer(unsigned int);
    ~EbmServer();

    void run();
    void stop();

    //void Broadcast();
    void OnReadable(const Poco::AutoPtr<Poco::Net::ReadableNotification>&);
private:
    static const bool DEBUG_ENABLED = false;

    bool running;
    unsigned int port;
    Poco::Net::DatagramSocket socket;
    Poco::Net::SocketReactor reactor;
    Poco::Thread thread;

    //NhdpProtoClient nhdp;
    //EbmProtoClient ebm;
};
#endif

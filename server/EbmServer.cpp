// EbmServer.cpp
//
#include "EbmServer.h"
#include "Poco/NObserver.h"
#include <iostream>
#include <cstdlib>
#include <unistd.h>

/*struct EbmMessage {
    int priority;
    std::string id;
    std::string address;
    bool visited;
};*/

EbmServer::EbmServer()
{
    port = 5514;
    running = false;
}

EbmServer::EbmServer(unsigned int p)
{
    port = p;
    running = false;
}

EbmServer::~EbmServer()
{
    //TODO: removeListener...
}

//TODO: int return value
void EbmServer::run()
{
    if (!running)
    {
        running = true;
        std::cout << "EBM Server starting on port " << port << "..." << std::endl;
        reactor.addEventHandler(socket, Poco::NObserver<EbmServer, Poco::Net::ReadableNotification>(*this, &EbmServer::OnReadable));
        //socket.setBroadcast(true);
        socket.bind(Poco::Net::SocketAddress(Poco::Net::IPAddress(), port));
        std::cout << "Reactor starting..." << std::endl;
        thread.start(reactor);
        while (running) {
            usleep(1000);
        }
        std::cout << "EBM Server stopping..." << std::endl;
    } else {
        std::cout << "EBM Server already running..." << std::endl;
    }
}

void EbmServer::stop()
{
    running = false;
}

/*void EbmServer::Broadcast()
{
    std::cout << "EBM Server broadcasting..." << std::endl;
}*/

void EbmServer::OnReadable(const Poco::AutoPtr<Poco::Net::ReadableNotification>&)
{
    char buffer[1024];
    Poco::Net::SocketAddress socketAddress;
    int length = socket.receiveFrom(buffer, sizeof(buffer)-1, socketAddress);
    buffer[length] = '\0';
    std::cout << "packet from " << socketAddress.toString() << ": " << buffer << std::endl;
    //TODO: process
    /*char *buffer = "test";
    socket.sendTo(buffer, 4, socketAddress);*/
}


// ECDS.cpp
//
#include "ECDS.h"
#include <stdio.h>
#include <stdlib.h>
#include <unistd.h>
#include <string.h>
#include <iostream>
#include <fstream>
#include <string>
#include <sstream>
#include <queue>
#include "../client/NhdpProtoClient.h"
//#include "../client/NhdpProtoClientNew.h"

ECDS::ECDS()
{
    
}

ECDS::~ECDS()
{
    
}

bool ECDS::run() {
    //TODO: move N1, N2 population here
    std::cout << "ECDS starting" << std::endl;

    // TODO: 1. initialize the set N1 with tuples (priority, id, neighbors) for each 1-hop neighbor of n0

    // 2. if N1 has less than 2 tuples, do not elect n0 as relay
    std::cout << "if N1 has less than 2 tuples, do not elect n0 as relay" << std::endl;
    std::cout << "\tN1 size: " << N1.size() << std::endl;
    if (N1.size() < 2) {
        return false;
    }

    // TODO: 3. initilize the set N2 with tuples (priority, id, 2-hop address) for each 2-hop neighbor of n0
    
    // 4. test n0 priority vs N1 union N2, elect n0 as relay if greater
    std::cout << "test n0 priority vs N1 union N2, elect n0 as relay if greater" << std::endl;
    std::cout << "\tn0 priority: " << n0.priority << std::endl;
    bool test = true;
    std::cout << "\tN1" << std::endl;
    for( std::vector<Router>::const_iterator n = N1.begin(); n != N1.end(); ++n)
    {
        std::cout << "\t\t" << (*n).id << ": " << (*n).priority << std::endl;
        if ((*n).priority >= n0.priority) {
            test = false;
            //break;
        }
    }
    std::cout << "\tN2" << std::endl;
    for( std::vector<Router>::const_iterator n = N2.begin(); n != N2.end(); ++n)
    {
        std::cout << "\t\t" << (*n).id << ": " << (*n).priority << std::endl;
        if ((*n).priority >= n0.priority) {
            test = false;
            //break;
        }
    }
    if (test) {
        return true;
    }

    // 5. initialize all tuples in U(N1,N2) as unvisited (done by default)

    // 6. find the tuple that has the largest priority in N1
    std::cout << "find tuple with largest priority in N1" << std::endl;
    Router n1_Max = *(N1.begin());
    for( std::vector<Router>::const_iterator n = N1.begin(); n != N1.end(); ++n)
    {
        if ((*n).priority > n1_Max.priority) {
            n1_Max = *n;
        }
    }
    std::cout << "\t" << n1_Max.id << std::endl;

    // 7. initiailize queue q to contain n1_Max and mark as visited
    std::cout << "initialize queue q to contain n1_Max and mark as visited" << std::endl;
    n1_Max.visited = true;
    for(unsigned int n=0; n < N1.size(); n++) {
        if (N1[n].id == n1_Max.id) {
            N1[n].visited = true;
        }
    }    
    std::queue<Router> q;
    q.push(n1_Max);

    // 8. while q is not empty
    std::cout << "while q is not empty" << std::endl;
    std::cout << "size: " << q.size() << std::endl;
    while (q.size() > 0) {
        //remove x from head of queue
        std::cout << "remove x from head of queue" << std::endl;
        Router x = q.front();
        std::cout << "\tX: " << x.id << std::endl;
        q.pop();
        std::cout << "\tq size: " << q.size() << std::endl;

        //for each 1-hop neighbor n of router x (excluding n0)
        std::cout << "\tfor each 1-hop neighbor n of x (excluding n0)" << std::endl;
        std::vector<std::string> one_hop_neighbors = nhdp.getOneHopNeighbors(x.address);
        for( std::vector<std::string>::const_iterator n = one_hop_neighbors.begin(); n != one_hop_neighbors.end(); ++n)
        {
            // excluding n0
            if ((*n).compare(n0.address) != 0) {
                std::cout << "\tn: " << *n << std::endl;
                // that is not marked as visited in local list
                for(unsigned int n_local=0; n_local < N1.size(); n_local++) {
                    std::cout << "\t\tn_local: " << N1[n_local].id << std::endl;
                    if ((*n).compare(N1[n_local].address) == 0) {
                        std::cout << "\t\t\tmatch in neighbors" << std::endl;
                        //if not visited
                        if (N1[n_local].visited == false) {
                            std::cout << "\t\t\tnot visited" << std::endl;
                            // 8A. mark as visited
                            N1[n_local].visited = true;

                            // 8B. if priority > n0
                            std::cout << "\t\t\tif priority > n0" << std::endl;
                            std::cout << "\t\t\t\t" << N1[n_local].priority << " : " << n0.priority << std::endl;
                            if (N1[n_local].priority > n0.priority) {
                                //append n to Q
                                std::cout << "\t\t\t\t\tappend n to q" << std::endl;
                                q.push(N1[n_local]);
                            }
                        }
                    }
                }
            }
        }
    }

    // 9. if any tuple in N1 remains unvisited, n0 selects itself as a relay
    //    otherwise, n0 does not act as a relay
    std::cout << "if any tuple in N1 remains unvisited, n0 selects itself as relay" << std::endl;
    std::cout << "otherwise, n0 does not act as a relay" << std::endl;
    test = false;
    for( std::vector<Router>::const_iterator n = N1.begin(); n != N1.end(); ++n)
    {
        std::cout << "\t" << (*n).id << std::endl;
        if ((*n).visited == false) {
            std::cout << "\t\tnot visited" << std::endl;
            test = true;
            //break;
        }
    }
    if (test) {
        std::cout << "RELAY" << std::endl;
        return true;
    }
    else {
        std::cout << "NOT RELAY" << std::endl;
        return false;
    }
}

void ECDS::setn0(Router n) {
    n0 = n;
}

void ECDS::setn0(int priority, std::string id, std::string address) {
    n0 = {priority, id, address, true};
}

void ECDS::setN1(std::vector<Router> n1) {
    N1 = n1;
}

void ECDS::addN1(Router n) {
    N1.push_back(n);
}

void ECDS::addN1(int priority, std::string id, std::string address) {
    Router n = {priority, id, address, false};
    N1.push_back(n);
}

void ECDS::setN2(std::vector<Router> n2) {
    N2 = n2;
}

void ECDS::addN2(Router n) {
    N2.push_back(n);
}

void ECDS::addN2(int priority, std::string id, std::string address) {
    Router n = {priority, id, address, false};
    N2.push_back(n);
}


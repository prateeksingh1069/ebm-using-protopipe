// SMPR.h
//
#ifndef SMPR_H
#define SMPR_H

#include <string>
#include <vector>
//#include "../client/NhdpProtoClientNew.h"
#include "../client/NhdpProtoClient.h"

/*struct Router {
    int priority;
    std::string id;
    std::string address;
    bool visited;
};*/

class SMPR
{
public:
    SMPR();
    ~SMPR();

    std::vector<Router> run();

    void setn0(Router n);
    void setn0(int priority, std::string id, std::string address);

    void setMPR(std::vector<Router> mpr);
    void addMPR(Router n);
    void addMPR(int priority, std::string id, std::string address);

    void setN1(std::vector<Router> n1);
    void addN1(Router n);
    void addN1(int priority, std::string id, std::string address);
    void addN1z(std::vector<Router> n);
    void addN1z(Router n, int z);
    void addN1z(int priority, std::string id, std::string address, int z);
    
    void setN2(std::vector<Router> n2);
    void addN2(Router n);
    void addN2(int priority, std::string id, std::string address);
    void addN2y(std::vector<Router> n);
    void addN2y(Router n, int y);
    void addN2y(int priority, std::string id, std::string address, int y);
private:
    static const bool DEBUG_ENABLED = false;

    #define NEVER 0
    #define ALWAYS 1

    Router n0;
    std::vector<Router> MPR, N1, N2;
    std::vector< std::vector<Router> > N1z;
    std::vector< std::vector<Router> > N2y;
    NhdpProtoClient nhdp;
};
#endif

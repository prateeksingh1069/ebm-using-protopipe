// SMPR.cpp
//
#include "SMPR.h"
#include <stdio.h>
#include <stdlib.h>
#include <unistd.h>
#include <string.h>
#include <iostream>
#include <fstream>
#include <string>
#include <sstream>
#include <queue>
#include "../client/NhdpProtoClient.h"
//#include "../client/NhdpProtoClientNew.h"

SMPR::SMPR()
{
    
}

SMPR::~SMPR()
{
    
}

std::vector<Router> SMPR::run() {
    //TODO: move N1, N2 population here
    std::cout << "S-MPR starting" << std::endl;

    // TODO: 1. initialize the set MPR to empty

    // TODO: 2. initialize the set N1 to include all 1-hop neighbors of n0

    // TODO: 3. initialize the set N2 to include all 2-hop neighbors, excluding n0 and any routers in N1
    //          nodes only reachable through N1 routers with NEVER priority are also excluded

    // 4. for each interface y in N1
    std::cout << "for each interface y in N1" << std::endl;
    for(unsigned int y=0; y < N1.size(); y++)
    {
        std::cout << "y: " << N1[y].address << std::endl;
        std::vector<Router> temp;
        //initialize N2(y) to include members of N2 that are 1-hop neighbors of y
        std::vector<std::string> one_hop_neighbors = nhdp.getOneHopNeighbors(N1[y].address);
        for(unsigned int n = 0; n < one_hop_neighbors.size(); n++)
        {
            std::cout << "\t1-hop: " << one_hop_neighbors[n] << std::endl; 
            // check for match in N2
            for(unsigned int y2=0; y2 < N2.size(); y2++)
            {
                std::cout << "\t\tn2: " << N2[y2].address << std::endl;
                if (one_hop_neighbors[n].compare(N2[y2].address) == 0)
                {
                    std::cout << "\t\t\tmatch" << std::endl;
                    temp.push_back(N2[y2]);
                }
            }
        }
        addN2y(temp);
    }

    // 5. for each interface x in N1
    for(unsigned int x = 0; x < N1.size(); x++)
    {
        //with a priority value of ALWAYS
        if (N1[x].priority == ALWAYS)
        {
            //select x as an MPR

            // 5A. add x to MPR and remove x from N1
            addMPR(N1[x]);
            //N1.erase(std::remove(N1.begin(), N1.end(), *x), N1.end());
            N1.erase(N1.begin() + x); //TODO: test

            // 5B. for each interface z in N2(x) remove z from N2
            for(unsigned int z = 0; z < N2y[x].size(); z++)
            {
                for(unsigned int z2 = 0; z2 < N2.size(); z2++)
                {
                    if ((N2y[x][z].address).compare(N2[z2].address) == 0)
                    {
                        N2.erase(N2.begin() + z2); //TODO: test
                    }
                }
            }

            // 5C. for each interface y in N1 remove any interfaces in N2(x) from N2(y)
            for(unsigned int y=0; y < N1.size(); y++)
            {
                if(y != x)
                {
                    for(unsigned int y2 = 0; y2 < N2y[y].size(); y2++)
                    {
                        for(unsigned int x2 = 0; x2 < N2y[x].size(); x2++)
                        {
                            if((N2y[y][y2].address).compare(N2y[x][x2].address) == 0)
                            {
                                N2y[y].erase(N2y[y].begin() + y2); //TODO: test
                            }
                        }
                    }
                }
            }
        }
    }

    // 6. for each interface z in N2
    for(unsigned int z=0; z < N2.size(); z++)
    {
        std::vector<Router> temp;
        // initialize N1(z) to include members of N1 that are 1-hop neighbors of z
        std::vector<std::string> one_hop_neighbors = nhdp.getOneHopNeighbors(N2[z].address);
        for(unsigned int n = 0; n < one_hop_neighbors.size(); n++)
        {
            // check for match in N1
            for(unsigned int z2=0; z2 < N1.size(); z2++)
            {
                if (one_hop_neighbors[n].compare(N1[z2].address) == 0)
                {
                    temp.push_back(N1[z2]);
                }
            }
        }
        addN1z(temp);
    }

    // 7. for each interface x in N2
    for(unsigned int x=0; x < N2.size(); x++)
    {
        // where N1(x) has only one member
        if(N1z[x].size() == 1)
        {
            //select x as an MPR

            // 7A. add x to MPR and remove x from N1
            addMPR(N2[x]);
            for(unsigned int x2 = 0; x2 < N1.size(); x2++)
            {
                if ((N1[x2].address).compare(N2[x].address) == 0)
                {
                    N1.erase(N1.begin() + x2); //TODO: test
                }
            }

            // 7B. for each interface z in N2(x) remove z from N2 and delete N1(z)
            for(unsigned int z = 0; z < N2y[x].size(); z++)
            {
                for(unsigned int z2 = 0; z2 < N2.size(); z2++)
                {
                    // remove z from N2
                    if ((N2y[x][z].address).compare(N2[z2].address) == 0)
                    {
                        N2.erase(N2.begin() + z2); //TODO: test
                        // delete N1(z)
                        std::vector<Router> temp;
                        N1z[z2] = temp;
                    }
                }
            }

            // 7C. for each interface y in N1 remove any interfaces in N2(x) from N2(y)
            for(unsigned int y=0; y < N1.size(); y++)
            {
                if((N1[y].address).compare(N2[x].address) != 0)
                {
                    for(unsigned int y2 = 0; y2 < N2y[y].size(); y2++)
                    {
                        for(unsigned int x2 = 0; x2 < N1.size(); x2++)
                        {
                            if ((N2[x].address).compare(N1[x2].address) == 0)
                            {
                                for(unsigned int x3 = 0; x3 < N2y[x2].size(); x3++)
                                {
                                    if((N2y[y][y2].address).compare(N2y[x2][x3].address) == 0)
                                    {
                                        N2y[y].erase(N2y[y].begin() + y2); //TODO: test
                                    }
                                }
                            }
                        }
                    }
                }
            }
        }
    }

    // 8. while N2 is not empty
    while(N2.size() > 0)
    {
        // select the interface x in N1 with the largest router priority that has members in N2(x)
        unsigned int x = 0;
        for (unsigned int x2 = 0; x2 < N1.size(); x2++)
        {
            if(N1[x2].priority > N1[x].priority)
            {
                x = x2;
            }
        }
        //select x as an MPR

        // 5A. add x to MPR and remove x from N1
        addMPR(N1[x]);
        //N1.erase(std::remove(N1.begin(), N1.end(), *x), N1.end());
        N1.erase(N1.begin() + x); //TODO: test

        // 5B. for each interface z in N2(x) remove z from N2
        for(unsigned int z = 0; z < N2y[x].size(); z++)
        {
            for(unsigned int z2 = 0; z2 < N2.size(); z2++)
            {
                if ((N2y[x][z].address).compare(N2[z2].address) == 0)
                {
                    N2.erase(N2.begin() + z2); //TODO: test
                }
            }
        }

        // 5C. for each interface y in N1 remove any interfaces in N2(x) from N2(y)
        for(unsigned int y=0; y < N1.size(); y++)
        {
            if(y != x)
            {
                for(unsigned int y2 = 0; y2 < N2y[y].size(); y2++)
                {
                    for(unsigned int x2 = 0; x2 < N2y[x].size(); x2++)
                    {
                        if((N2y[y][y2].address).compare(N2y[x][x2].address) == 0)
                        {
                            N2y[y].erase(N2y[y].begin() + y2); //TODO: test
                        }
                    }
                }
            }
        }
    }

    return MPR;
}

void SMPR::setn0(Router n) {
    n0 = n;
}

void SMPR::setn0(int priority, std::string id, std::string address) {
    n0 = {priority, id, address, true};
}

void SMPR::setMPR(std::vector<Router> mpr) {
    MPR = mpr;
}

void SMPR::addMPR(Router n) {
    MPR.push_back(n);
}

void SMPR::addMPR(int priority, std::string id, std::string address) {
    Router n = {priority, id, address, false};
    MPR.push_back(n);
}

void SMPR::setN1(std::vector<Router> n1) {
    N1 = n1;
}

void SMPR::addN1(Router n) {
    N1.push_back(n);
}

void SMPR::addN1(int priority, std::string id, std::string address) {
    Router n = {priority, id, address, false};
    N1.push_back(n);
}

void SMPR::addN1z(std::vector<Router> n) {
    N1z.push_back(n);
}

void SMPR::addN1z(Router n, int z) {
    N1z[z].push_back(n);
}

void SMPR::addN1z(int priority, std::string id, std::string address, int z) {
    Router n = {priority, id, address, false};
    N1z[z].push_back(n);
}

void SMPR::setN2(std::vector<Router> n2) {
    N2 = n2;
}

void SMPR::addN2(Router n) {
    N2.push_back(n);
}

void SMPR::addN2(int priority, std::string id, std::string address) {
    Router n = {priority, id, address, false};
    N2.push_back(n);
}

void SMPR::addN2y(std::vector<Router> n) {
    N2y.push_back(n);
}

void SMPR::addN2y(Router n, int y) {
    N2y[y].push_back(n);
}

void SMPR::addN2y(int priority, std::string id, std::string address, int y) {
    Router n = {priority, id, address, false};
    N2y[y].push_back(n);
}


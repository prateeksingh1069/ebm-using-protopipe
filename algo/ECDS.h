// ECDS.h
//
#ifndef ECDS_H
#define ECDS_H

#include <string>
#include <vector>
//#include "../client/NhdpProtoClientNew.h"
#include "../client/NhdpProtoClient.h"

/*struct Router {
    int priority;
    std::string id;
    std::string address;
    bool visited;
};*/

class ECDS
{
public:
    ECDS();
    ~ECDS();

    bool run();

    void setn0(Router n);
    void setn0(int priority, std::string id, std::string address);

    void setN1(std::vector<Router> n1);
    void addN1(Router n);
    void addN1(int priority, std::string id, std::string address);
    
    void setN2(std::vector<Router> n2);
    void addN2(Router n);
    void addN2(int priority, std::string id, std::string address);
private:
    static const bool DEBUG_ENABLED = false;

    Router n0;
    std::vector<Router> N1, N2;
    NhdpProtoClient nhdp;
};
#endif

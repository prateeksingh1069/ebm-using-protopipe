/* UDP client in the internet domain */
#include <sys/types.h>
#include <sys/socket.h>
#include <netinet/in.h>
#include <arpa/inet.h>
#include <netdb.h>
#include <stdio.h>
#include <stdlib.h>
#include <unistd.h>
#include <string.h>
#include <iostream>
#include <fstream>
#include <string>
#include <sstream>
//#include "protolib/src/common/protoAddress.cpp"
//#include "protolib/src/common/protoSocket.cpp"
#include "../proto/nhdp.pb.h"
#include "Poco/Net/DatagramSocket.h"
#include "Poco/Net/SocketAddress.h"


using Poco::Net::DatagramSocket;
using Poco::Net::SocketAddress;
using Poco::UInt16;

int main(int argc, char *argv[])
{
	char buf[8192];
    unsigned int size = 8192;
    Poco::UInt16 NHDP_PORT = 5555;
    //ProtoAddress addr;

    SocketAddress addr(argv[1], NHDP_PORT);
    SocketAddress sa;

    //addr.ResolveFromString(argv[1]);
    //addr.SetPort(5555);

	//ProtoSocket socket2(ProtoSocket::UDP);

    DatagramSocket socket;

	Manet::TwoHopNeighborSetMessage neighborMessage2;

    Manet::NhdpMessage request2;
    Manet::NhdpMessageHeader *header2 = request2.mutable_header();
    header2->set_messagetype(Manet::TWO_HOP_NEIGHBOR_SET);
    header2->set_optype(Manet::GET);
    request2.set_message(neighborMessage2.SerializeAsString());

    if(socket.sendTo(request2.SerializeAsString().c_str(), request2.ByteSize(), addr))
    {
        std::cout << "successful send" << std::endl << std::endl;

        if(socket.receiveFrom(buf,size,sa))
        {
            std::cout << "successfully recieved from NHDP" << std::endl;
            Manet::NhdpMessage response2;
            response2.ParseFromArray(buf,size);
            neighborMessage2.ParseFromString(response2.message());
            std::cout << "neighborMessage2.neighbors_size() : " << neighborMessage2.neighbors_size() << std::endl;

            if(0 == neighborMessage2.neighbors_size())
            {
                std::cout << "Zero neighbors." << std::endl;
            }

            for( int i = 0; i < neighborMessage2.neighbors_size(); ++i)
            {
                for( int j = 0; j < neighborMessage2.neighbors(i).onehopneighboraddress_size(); ++j)
                {
                    std::cout << "One Hop Addrs:" << std::endl;
                    std::cout << neighborMessage2.neighbors(i).onehopneighboraddress(j) << std::endl;
                }
                if( neighborMessage2.neighbors(i).has_twohopaddress() )
                    std::cout << "Two Hop Addr: " << neighborMessage2.neighbors(i).twohopaddress() << std::endl;
                if( neighborMessage2.neighbors(i).has_expiration() )
                    std::cout << "Expires: " << neighborMessage2.neighbors(i).expiration();

                std::cout << std::endl << std::endl;
            }
        }
    }
    else
    {
        std::cout << "send to socket failed" << std::endl;
    }

	socket.close();

    return 0;

}

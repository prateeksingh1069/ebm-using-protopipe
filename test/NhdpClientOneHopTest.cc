/* UDP client in the internet domain */
#include <sys/types.h>
#include <sys/socket.h>
#include <netinet/in.h>
#include <arpa/inet.h>
#include <netdb.h>
#include <stdio.h>
#include <stdlib.h>
#include <unistd.h>
#include <string.h>
#include <iostream>
#include <fstream>
#include <string>
#include <sstream>
#include "../../protolib/include/protoAddress.h"
#include "../../protolib/include/protoSocket.h"
#include "../proto/nhdp.pb.h"


int main(int argc, char *argv[])
{
	char buf[8192];
    unsigned int size = 8192;
    ProtoAddress addr;
    addr.ResolveFromString(argv[1]);
    addr.SetPort(5555);

    ProtoSocket socket(ProtoSocket::UDP);
	
    Manet::NeighborSetMessage neighborMessage;

    Manet::NhdpMessage request;
    Manet::NhdpMessageHeader *header = request.mutable_header();
    header->set_messagetype(Manet::NEIGHBOR_SET);
    header->set_optype(Manet::GET);
    request.set_message(neighborMessage.SerializeAsString());

    if(socket.SendTo(request.SerializeAsString().c_str(), request.ByteSize(), addr))
    {
        std::cout << "successful send" << std::endl;

        if(socket.Recv(buf,size))
        {
            Manet::NhdpMessage response;
            response.ParseFromArray(buf,size);
            neighborMessage.ParseFromString(response.message());

            for( int i = 0; i < neighborMessage.neighbors_size(); ++i)
            {
                std::cout << neighborMessage.neighbors(i).addresses(0).address() << " symetric: ";
                if(neighborMessage.neighbors(i).has_status() && neighborMessage.neighbors(i).status() == Manet::NeighborMessage_NeighborStatus_SYMMETRIC)
                    std::cout << "true";
                else
                    std::cout << "false";

                std::cout << std::endl;
            }
        }
    }
    else
    {
        std::cout << "send to socket failed" << std::endl;
    }
	socket.Close();
	
    return 0;
	
}

/* Creates a datagram server.  The port 
   number is passed as an argument.  This
   server runs forever */

#include <sys/types.h>
#include <stdlib.h>
#include <unistd.h>
#include <sys/socket.h>
#include <netinet/in.h>
#include <string.h>
#include <netdb.h>
#include <stdio.h>
#include <iostream>
#include <fstream>
#include <string>
#include "../proto/ebm.pb.h"

using namespace std;

void error(const char *);
Manet::EbmMessage create_message(Manet::ManetOperationalType optype, Manet::EbmMessageType messagetype);
Manet::EbmRelayMessage create_relay_message(Manet::ManetOperationalType optype, Manet::EbmMessageType messagetype);
void print_message(Manet::EbmMessage msg);
void print_relay_message(Manet::EbmRelayMessage msg);
string serialize_message(Manet::EbmMessage msg);
string serialize_relay_message(Manet::EbmRelayMessage msg);
Manet::EbmMessage parse_message(string serial);
Manet::EbmRelayMessage parse_relay_message(string serial);

int main(int argc, char *argv[])
{
   int sock, length, n;
   socklen_t fromlen;
   struct sockaddr_in server;
   struct sockaddr_in from;
   int port = 5553;
   char buf[1024];

   /*if (argc < 2) {
      fprintf(stderr, "ERROR, no port provided\n");
      exit(0);
   }*/
   
   sock=socket(AF_INET, SOCK_DGRAM, 0);
   if (sock < 0) error("Opening socket");
   length = sizeof(server);
   bzero(&server,length);
   server.sin_family=AF_INET;
   server.sin_addr.s_addr=INADDR_ANY;
   server.sin_port=htons(port);
   if (bind(sock,(struct sockaddr *)&server,length)<0) 
      error("binding");
   fromlen = sizeof(struct sockaddr_in);
   cout << "listening on port " << port << endl;

   while (1) {
      n = recvfrom(sock,buf,1024,0,(struct sockaddr *)&from,&fromlen);
      if (n < 0) error("recvfrom");
      cout << "received:" << endl;

      //parse
      string data (buf);
      Manet::EbmMessage ebm_message = parse_message(data);
      assert(ebm_message.header().responsecode() == Manet::SUCCESSFUL);
      print_message(ebm_message);

      //process
      if (ebm_message.header().messagetype() == Manet::RELAY)
      {
         Manet::EbmRelayMessage ebm_response;
         ebm_response = create_relay_message(Manet::INFO, Manet::RELAY);
         ebm_response.set_routerid(1);
         ebm_response.set_isrelay(true);
         cout << "sending:" << endl;
         print_relay_message(ebm_response);

         //serialize
         string resp = serialize_relay_message(ebm_response);
   
         //send
         n=sendto(sock,resp.data(),resp.length(),0,(struct sockaddr *)&from,fromlen);
         if (n < 0) error("sendto");
      }
      else
      {
         Manet::EbmMessage ebm_response;
         ebm_response = create_message(Manet::INFO, Manet::ERROR);
         ebm_response.mutable_header()->set_responsecode(Manet::NOT_IMPLEMENTED);
         ebm_response.set_message("OTHER");
         cout << "sending:" << endl;
         print_message(ebm_response);

         //serialize
         string resp = serialize_message(ebm_response);
   
         //send
         n=sendto(sock,resp.data(),resp.length(),0,(struct sockaddr *)&from,fromlen);
         if (n < 0) error("sendto");
      }
   }
   return 0;
}

void error(const char *msg)
{
    perror(msg);
    exit(0);
}

Manet::EbmMessage create_message(Manet::ManetOperationalType optype, Manet::EbmMessageType messagetype)
{
   Manet::EbmMessage msg;
   msg.mutable_header()->set_optype(optype);
   msg.mutable_header()->set_messagetype(messagetype);
   //msg.mutable_header()->set_responsecode(Manet::SUCCESSFUL);
   //msg.set_message("test");
   return msg;
}

Manet::EbmRelayMessage create_relay_message(Manet::ManetOperationalType optype, Manet::EbmMessageType messagetype)
{
   Manet::EbmRelayMessage msg;
   msg.mutable_header()->set_optype(optype);
   msg.mutable_header()->set_messagetype(messagetype);
   //msg.mutable_header()->set_responsecode(Manet::SUCCESSFUL);
   //msg.set_message("test");
   return msg;
}

void print_message(Manet::EbmMessage msg)
{
   cout << "[ ";
   if (msg.has_header())
   {
      cout << Manet::ManetOperationalType_Name(msg.header().optype()) << " | " << Manet::EbmMessageType_Name(msg.header().messagetype()) << " | " << Manet::ManetResponseCode_Name(msg.header().responsecode());
   }
   cout << " ] { ";
   if (msg.has_message())
   {
      cout << msg.message();
   }
   cout << " }" << endl;
}

void print_relay_message(Manet::EbmRelayMessage msg)
{
   cout << "[ ";
   if (msg.has_header())
   {
      cout << Manet::ManetOperationalType_Name(msg.header().optype()) << " | " << Manet::EbmMessageType_Name(msg.header().messagetype()) << " | " << Manet::ManetResponseCode_Name(msg.header().responsecode());
   }
   cout << " ] { ";
   cout << msg.routerid();
   cout << " } { ";
   cout << msg.isrelay();
   cout << " }" << endl;
}

string serialize_message(Manet::EbmMessage msg)
{
   string serial;
   msg.SerializeToString(&serial);
   return serial;
}

string serialize_relay_message(Manet::EbmRelayMessage msg)
{
   string serial;
   msg.SerializeToString(&serial);
   return serial;
}

Manet::EbmMessage parse_message(string serial)
{
   Manet::EbmMessage msg;
   msg.ParseFromString(serial);
   return msg;
}

Manet::EbmRelayMessage parse_relay_message(string serial)
{
   Manet::EbmRelayMessage msg;
   msg.ParseFromString(serial);
   return msg;
}


/* Creates a datagram server.  The port 
   number is passed as an argument.  This
   server runs forever */

#include <sys/types.h>
#include <stdlib.h>
#include <unistd.h>
#include <sys/socket.h>
#include <netinet/in.h>
#include <string.h>
#include <netdb.h>
#include <stdio.h>
#include <iostream>
#include <fstream>
#include <string>
#include "../proto/nhdp.pb.h"

using namespace std;

void error(const char *);
Manet::NhdpMessage create_message(Manet::ManetOperationalType optype, Manet::NhdpMessageType messagetype);
void print_message(Manet::NhdpMessage msg);
string serialize_message(Manet::NhdpMessage msg);
Manet::NhdpMessage parse_message(string serial);

int main(int argc, char *argv[])
{
   int sock, length, n;
   socklen_t fromlen;
   struct sockaddr_in server;
   struct sockaddr_in from;
   int port = 5555;
   char buf[1024];

   /*if (argc < 2) {
      fprintf(stderr, "ERROR, no port provided\n");
      exit(0);
   }*/
   
   sock=socket(AF_INET, SOCK_DGRAM, 0);
   if (sock < 0) error("Opening socket");
   length = sizeof(server);
   bzero(&server,length);
   server.sin_family=AF_INET;
   server.sin_addr.s_addr=INADDR_ANY;
   server.sin_port=htons(port);
   if (bind(sock,(struct sockaddr *)&server,length)<0) 
      error("binding");
   fromlen = sizeof(struct sockaddr_in);
   cout << "listening on port " << port << endl;

   while (1) {
      n = recvfrom(sock,buf,1024,0,(struct sockaddr *)&from,&fromlen);
      if (n < 0) error("recvfrom");
      cout << "received:" << endl;

      //parse
      string data (buf);
      Manet::NhdpMessage nhdp_message = parse_message(data);
      assert(nhdp_message.header().responsecode() == Manet::SUCCESSFUL);
      print_message(nhdp_message);

      //process
      Manet::NhdpMessage nhdp_response;
      if (nhdp_message.header().messagetype() == Manet::NEIGHBOR_SET)
      {
         nhdp_response = create_message(Manet::INFO, Manet::NEIGHBOR_SET);
         nhdp_response.set_message("10.0.0.1 10.0.0.2");
      }
      else if (nhdp_message.header().messagetype() == Manet::TWO_HOP_NEIGHBOR_SET)
      {
         nhdp_response = create_message(Manet::INFO, Manet::TWO_HOP_NEIGHBOR_SET);
         nhdp_response.set_message("10.0.0.2");
      }
      else
      {
         nhdp_response = create_message(Manet::INFO, Manet::ROUTER);
         nhdp_response.mutable_header()->set_responsecode(Manet::NOT_IMPLEMENTED);
         nhdp_response.set_message("OTHER");
      }
      cout << "sending:" << endl;
      print_message(nhdp_response);

      //serialize
      string resp = serialize_message(nhdp_response);
   
      //send
      n=sendto(sock,resp.data(),resp.length(),0,(struct sockaddr *)&from,fromlen);
      if (n < 0) error("sendto");
   }
   return 0;
}

void error(const char *msg)
{
    perror(msg);
    exit(0);
}

Manet::NhdpMessage create_message(Manet::ManetOperationalType optype, Manet::NhdpMessageType messagetype)
{
   Manet::NhdpMessage msg;
   msg.mutable_header()->set_optype(optype);
   msg.mutable_header()->set_messagetype(messagetype);
   //msg.mutable_header()->set_responsecode(Manet::SUCCESSFUL);
   //msg.set_message("test");
   return msg;
}

void print_message(Manet::NhdpMessage msg)
{
   cout << "[ ";
   if (msg.has_header())
   {
      cout << Manet::ManetOperationalType_Name(msg.header().optype()) << " | " << Manet::NhdpMessageType_Name(msg.header().messagetype()) << " | " << Manet::ManetResponseCode_Name(msg.header().responsecode());
   }
   cout << " ] { ";
   if (msg.has_message())
   {
      cout << msg.message();
   }
   cout << " }" << endl;
}

string serialize_message(Manet::NhdpMessage msg)
{
   string serial;
   msg.SerializeToString(&serial);
   return serial;
}

Manet::NhdpMessage parse_message(string serial)
{
   Manet::NhdpMessage msg;
   msg.ParseFromString(serial);
   return msg;
}


/* UDP client in the internet domain */
#include <sys/types.h>
#include <sys/socket.h>
#include <netinet/in.h>
#include <arpa/inet.h>
#include <netdb.h>
#include <stdio.h>
#include <stdlib.h>
#include <unistd.h>
#include <string.h>
#include <iostream>
#include <fstream>
#include <string>
#include <sstream>
#include "../proto/ebm.pb.h"

using namespace std;

inline bool isValid(char ch) {
   //return (ch >= 'A' && ch <= 'Z') || (ch >= 'a' && ch <= 'z');
   return (ch >= ' ' && ch <= '9');
}

void strip(std::string const& s, std::string* result)
{
    result->clear();
    if (s.empty()) {
        return;
    }
    result->resize(s.size());

    char ch;
    char const* p = s.c_str();
    char const* e = p + s.size();
    char* o = &(*result)[0];
    char const* r = o;
    for (; p < e; ++p) {
        ch = *p;
        if (isValid(ch)) {
            *o++ = (ch);
        }
    }
    result->resize(o - r);
}

void error(const char *);
Manet::EbmMessage create_message(Manet::ManetOperationalType optype, Manet::EbmMessageType messagetype);
Manet::EbmRelayMessage create_relay_message(Manet::ManetOperationalType optype, Manet::EbmMessageType messagetype);
void print_message(Manet::EbmMessage msg);
void print_relay_message(Manet::EbmRelayMessage msg);
string serialize_message(Manet::EbmMessage msg);
string serialize_relay_message(Manet::EbmRelayMessage msg);
Manet::EbmMessage parse_message(string serial);
Manet::EbmRelayMessage parse_relay_message(string serial);

int main(int argc, char *argv[])
{
   int sock, n;
   unsigned int length;
   struct sockaddr_in server, from;
   struct hostent *hp;
   int port = 5553;
   char buffer[256];
   
   if (argc != 2) { printf("Usage: host\n");
                    exit(1);
   }
   sock= socket(AF_INET, SOCK_DGRAM, 0);
   if (sock < 0) error("socket");

   server.sin_family = AF_INET;
   hp = gethostbyname(argv[1]);
   if (hp==0) error("Unknown host");

   bcopy((char *)hp->h_addr, 
        (char *)&server.sin_addr,
         hp->h_length);
   server.sin_port = htons(port);
   length=sizeof(struct sockaddr_in);

   //RELAY
   cout << "RELAY" << endl;
   //create message
   Manet::EbmMessage ebm_message = create_message(Manet::GET, Manet::RELAY);
   ebm_message.set_message("TEST");
   cout << "sending: ";
   print_message(ebm_message);

   //serialize
   string data = serialize_message(ebm_message);
   
   //send
   n=sendto(sock,data.data(),data.length(),0,(const struct sockaddr *)&server,length);
   if (n < 0) error("sendto");
   
   //get response
   n = recvfrom(sock,buffer,256,0,(struct sockaddr *)&from, &length);
   if (n < 0) error("recvfrom");

   //parse
   string resp (buffer);
   Manet::EbmRelayMessage ebm_response = parse_relay_message(resp);
   assert(ebm_response.header().responsecode() == Manet::SUCCESSFUL);
   cout << "received: ";
   print_relay_message(ebm_response);
   //cout << "ByteSize: " << nhdp_response.ByteSize() << endl;

   bool isrelay = ebm_response.isrelay();
   cout << "is relay: " << isrelay << endl;

   close(sock);
   return 0;
}

void error(const char *msg)
{
    perror(msg);
    exit(0);
}

Manet::EbmMessage create_message(Manet::ManetOperationalType optype, Manet::EbmMessageType messagetype)
{
   Manet::EbmMessage msg;
   msg.mutable_header()->set_optype(optype);
   msg.mutable_header()->set_messagetype(messagetype);
   //msg.mutable_header()->set_responsecode(Manet::SUCCESSFUL);
   //msg.set_message("test");
   return msg;
}

Manet::EbmRelayMessage create_relay_message(Manet::ManetOperationalType optype, Manet::EbmMessageType messagetype)
{
   Manet::EbmRelayMessage msg;
   msg.mutable_header()->set_optype(optype);
   msg.mutable_header()->set_messagetype(messagetype);
   //msg.mutable_header()->set_responsecode(Manet::SUCCESSFUL);
   //msg.set_message("test");
   return msg;
}

void print_message(Manet::EbmMessage msg)
{
   cout << "[ ";
   if (msg.has_header())
   {
      cout << Manet::ManetOperationalType_Name(msg.header().optype()) << " | " << Manet::EbmMessageType_Name(msg.header().messagetype()) << " | " << Manet::ManetResponseCode_Name(msg.header().responsecode());
   }
   cout << " ] { ";
   if (msg.has_message())
   {
      cout << msg.message();
   }
   cout << " }" << endl;
}

void print_relay_message(Manet::EbmRelayMessage msg)
{
   cout << "[ ";
   if (msg.has_header())
   {
      cout << Manet::ManetOperationalType_Name(msg.header().optype()) << " | " << Manet::EbmMessageType_Name(msg.header().messagetype()) << " | " << Manet::ManetResponseCode_Name(msg.header().responsecode());
   }
   cout << " ] { ";
   cout << msg.routerid();
   cout << " } { ";
   cout << msg.isrelay();
   cout << " }" << endl;
}

string serialize_message(Manet::EbmMessage msg)
{
   string serial;
   msg.SerializeToString(&serial);
   return serial;
}

string serialize_relay_message(Manet::EbmRelayMessage msg)
{
   string serial;
   msg.SerializeToString(&serial);
   return serial;
}

Manet::EbmMessage parse_message(string serial)
{
   Manet::EbmMessage msg;
   msg.ParseFromString(serial);
   return msg;
}

Manet::EbmRelayMessage parse_relay_message(string serial)
{
   Manet::EbmRelayMessage msg;
   msg.ParseFromString(serial);
   return msg;
}


#include "Poco/Net/DatagramSocket.h"
#include "Poco/Net/SocketAddress.h"
#include "Poco/Net/SocketReactor.h"
#include "Poco/Net/SocketNotification.h"
#include "Poco/Net/SocketAcceptor.h"
#include "Poco/Net/SocketConnector.h"
#include "Poco/Net/ServerSocket.h"
#include "Poco/Net/StreamSocket.h"
#include "Poco/Thread.h"
#include "Poco/Observer.h"
#include "Poco/NObserver.h"
#include "Poco/Util/ServerApplication.h"
#include <iostream>
#include <cstdlib>
#include <unistd.h>

class Broadcaster {
    public:
        Broadcaster(unsigned int port)
        {
            reactor.addEventHandler(socket, Poco::NObserver<Broadcaster, Poco::Net::ReadableNotification>(*this, &Broadcaster::OnReadable));
            //socket.setBroadcast(true);
            socket.bind(Poco::Net::SocketAddress(Poco::Net::IPAddress(), port));
            std::cout << "STARTING..." << std::endl;
            thread.start(reactor);

            //waitForTerminationRequest();
            //reactor.stop();
            //thread.join();
            //return Poco::Util::Application::EXIT_OK;
        }

        /*~Broadcaster()
        {
            std::cout << "STOPPING..." << std::endl;
        }*/

        void Broadcast()
        {
            std::cout << "broadcast" << std::endl;
        }

        void OnReadable(const Poco::AutoPtr<Poco::Net::ReadableNotification>&)
        {
            char buffer[1024];
            Poco::Net::SocketAddress socketAddress;
            int length = socket.receiveFrom(buffer, sizeof(buffer)-1, socketAddress);
            buffer[length] = '\0';
            std::cout << "packet from " << socketAddress.toString() << ": " << buffer << std::endl;
            //TODO: process
            /*char *buffer = "test";
            socket.sendTo(buffer, 4, socketAddress);*/
        }

    private:
        Poco::Net::DatagramSocket socket;
        Poco::Net::SocketReactor reactor;
        Poco::Thread thread;
};

int main(int argc, char *argv[])
{
    unsigned short port = 5514;
    Broadcaster broadcast(port);
    while (1) {
        usleep(1000);
    }

    return 0;
}

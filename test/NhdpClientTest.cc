/* UDP client in the internet domain */
#include <sys/types.h>
#include <sys/socket.h>
#include <netinet/in.h>
#include <arpa/inet.h>
#include <netdb.h>
#include <stdio.h>
#include <stdlib.h>
#include <unistd.h>
#include <string.h>
#include <iostream>
#include <fstream>
#include <string>
#include <sstream>
#include "../proto/nhdp.pb.h"

using namespace std;

inline bool isValid(char ch) {
   //return (ch >= 'A' && ch <= 'Z') || (ch >= 'a' && ch <= 'z');
   return (ch >= ' ' && ch <= '9');
}

void strip(std::string const& s, std::string* result)
{
    result->clear();
    if (s.empty()) {
        return;
    }
    result->resize(s.size());

    char ch;
    char const* p = s.c_str();
    char const* e = p + s.size();
    char* o = &(*result)[0];
    char const* r = o;
    for (; p < e; ++p) {
        ch = *p;
        if (isValid(ch)) {
            *o++ = (ch);
        }
    }
    result->resize(o - r);
}

void error(const char *);
Manet::NhdpMessage create_message(Manet::ManetOperationalType optype, Manet::NhdpMessageType messagetype);
void print_message(Manet::NhdpMessage msg);
string serialize_message(Manet::NhdpMessage msg);
Manet::NhdpMessage parse_message(string serial);

int main(int argc, char *argv[])
{
   int sock, n;
   unsigned int length;
   struct sockaddr_in server, from;
   struct hostent *hp;
   int port = 5555;
   char buffer[256];
   
   if (argc != 2) { printf("Usage: host\n");
                    exit(1);
   }
   sock= socket(AF_INET, SOCK_DGRAM, 0);
   if (sock < 0) error("socket");

   server.sin_family = AF_INET;
   hp = gethostbyname(argv[1]);
   if (hp==0) error("Unknown host");

   bcopy((char *)hp->h_addr, 
        (char *)&server.sin_addr,
         hp->h_length);
   server.sin_port = htons(port);
   length=sizeof(struct sockaddr_in);

   //ONE-HOP
   cout << "ONE-HOP NEIGHBORS" << endl;
   //create message
   Manet::NhdpMessage nhdp_message = create_message(Manet::GET, Manet::NEIGHBOR_SET);
   cout << "sending: ";
   print_message(nhdp_message);

   //serialize
   string data = serialize_message(nhdp_message);
   
   //send
   n=sendto(sock,data.data(),data.length(),0,(const struct sockaddr *)&server,length);
   if (n < 0) error("sendto");
   
   //get response
   n = recvfrom(sock,buffer,256,0,(struct sockaddr *)&from, &length);
   if (n < 0) error("recvfrom");

   //parse
   string resp (buffer);
   Manet::NhdpMessage nhdp_response = parse_message(resp);
   assert(nhdp_response.header().responsecode() == Manet::SUCCESSFUL);
   cout << "received: ";
   print_message(nhdp_response);
   //cout << "ByteSize: " << nhdp_response.ByteSize() << endl;

   //string msg = nhdp_response.message();
   //cout << "Message length: " << msg.length() << endl;
   string msg = "";
   strip(nhdp_response.message(), &msg);
   cout << "one-hop neighbors: " << msg << endl;

   stringstream msg_neighbors(msg);
   string neighbor;
   vector<std::string> neighbors;
   while(std::getline(msg_neighbors, neighbor, ' '))
   {
      neighbors.push_back(neighbor);
   }
   cout << "one-hop neighbor count: " << neighbors.size() << endl;

   //TWO-HOP
   cout << "TWO-HOP NEIGHBORS" << endl;
   //create message
   nhdp_message = create_message(Manet::GET, Manet::TWO_HOP_NEIGHBOR_SET);
   cout << "sending: ";
   print_message(nhdp_message);

   //serialize
   data = serialize_message(nhdp_message);
   
   //send
   n=sendto(sock,data.data(),data.length(),0,(const struct sockaddr *)&server,length);
   if (n < 0) error("sendto");
   
   //get response
   n = recvfrom(sock,buffer,256,0,(struct sockaddr *)&from, &length);
   if (n < 0) error("recvfrom");

   //parse
   string resp2 (buffer);
   nhdp_response = parse_message(resp2);
   assert(nhdp_response.header().responsecode() == Manet::SUCCESSFUL);
   cout << "received: ";
   print_message(nhdp_response);

   string msg2 = "";
   strip(nhdp_response.message(), &msg2);
   cout << "two-hop neighbors: " << msg2 << endl;

   stringstream msg_neighbors2(msg2);
   string neighbor2;
   vector<std::string> neighbors2;
   while(std::getline(msg_neighbors2, neighbor2, ' '))
   {
      neighbors2.push_back(neighbor2);
   }
   cout << "two-hop neighbor count: " << neighbors2.size() << endl;

   close(sock);
   return 0;
}

void error(const char *msg)
{
    perror(msg);
    exit(0);
}

Manet::NhdpMessage create_message(Manet::ManetOperationalType optype, Manet::NhdpMessageType messagetype)
{
   Manet::NhdpMessage msg;
   msg.mutable_header()->set_optype(optype);
   msg.mutable_header()->set_messagetype(messagetype);
   //msg.mutable_header()->set_responsecode(Manet::SUCCESSFUL);
   //msg.set_message("test");
   return msg;
}

void print_message(Manet::NhdpMessage msg)
{
   cout << "[ ";
   if (msg.has_header())
   {
      cout << Manet::ManetOperationalType_Name(msg.header().optype()) << " | " << Manet::NhdpMessageType_Name(msg.header().messagetype()) << " | " << Manet::ManetResponseCode_Name(msg.header().responsecode());
   }
   cout << " ] { ";
   if (msg.has_message())
   {
      cout << msg.message();
   }
   cout << " }" << endl;
}

string serialize_message(Manet::NhdpMessage msg)
{
   string serial;
   msg.SerializeToString(&serial);
   return serial;
}

Manet::NhdpMessage parse_message(string serial)
{
   Manet::NhdpMessage msg;
   msg.ParseFromString(serial);
   return msg;
}


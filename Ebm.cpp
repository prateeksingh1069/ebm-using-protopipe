// test.cpp
//
#include <sys/types.h>
#include <sys/socket.h>
#include <netinet/in.h>
#include <arpa/inet.h>
#include <netdb.h>
#include <iostream>
#include <stdio.h>
#include <stdlib.h>
#include <unistd.h>
#include <string.h>
#include <fstream>
#include <string>
#include <sstream>
#include <vector>
#include "client/NhdpProtoClient.h"
//#include "client/NhdpProtoClientNew.h"
#include "algo/ECDS.h"
#include "algo/SMPR.h"
#include "client/EbmProtoClient.h"
#include "server/EbmServer.h"
#include "protolib/include/protoPipe.h"
#include "protolib/include/protokit.h"
//#include "protolib/include/manetMsg.h"
#include <cstdint>
#include <ctype.h>
//#include "protolib/include/protoAddress.h"
//#include "protolib/include/protoSocket.h"
//#include "proto/nhdp.pb.h"


    enum floodingType
    {
        SUB_TYPE_SMF_CF                = 0, //RFC6621
        SUB_TYPE_SMF_SMPR              = 1, //RFC6621
        SUB_TYPE_SMF_ECDS              = 2, //RFC6621
        SUB_TYPE_SMF_MPRCDS            = 3,  //RFC6621
        SUB_TYPE_SMF_ECDS_ETX          = 128 //TBD metric/ETX document
    };


void SendForwardingInfo(bool h, char *);
bool CalcRelaySelectionAlgo(std::string a, std::string b);
bool selected;
ProtoPipe ebm_pipe(ProtoPipe::MESSAGE);
bool floodingOn;
bool isRelay;
char ebmPipeName[256];
//UINT8 floodingType;

int main(int argc, char *argv[])
{
    if (argc != 5) {
        std::cout << "Usage: host algo{ecds,smpr} pipeIdentifier{-ebmClient} pipeName{n1_smf, n2_smf etc.} " << std::endl;
        exit(1);
    }
    std::string host = argv[1];
    std::string algo = argv[2];

	//////////////////////////////////////////////////////////////////////////////////// Added by Prateek

	char ebmPipeName[256];
	char recvPipeName[256];
	strncpy(ebmPipeName,"nrlsmf",256); // Default send pipe name
	strncpy(recvPipeName,"Ebm",256); // Default receiving pipe name
	UINT8 floodingType = SUB_TYPE_SMF_ECDS ; // Default floodingType
	ProtoPipe ebm_pipe(ProtoPipe::MESSAGE);
	bool connect = false;

	for(int i = 3;i<argc;i++)
    {


		if((!strcmp(argv[i], "ebmClient")) || (!strcmp(argv[i], "-ebmClient"))) //set up sendPipeName and open smf_pipe (used to send mac layer mpr info to smfClient)
			{
				i++;
				if(ebm_pipe.IsOpen()) ebm_pipe.Close();
				strncpy(ebmPipeName,argv[i],strlen(argv[i]));// the ebm pipe name to connect to

				do
				{

				if(ebm_pipe.Connect(ebmPipeName))
				{
					connect = true;
					std::cout << "Connected to : " << argv[i] << std::endl;
					char cmd[256];
					cmd[255] = '\0';
					strcpy(cmd,"ebmServerStart ");
					strncat(cmd, recvPipeName, 255-strlen(cmd));
					unsigned int len = strlen(cmd);
					if (ebm_pipe.Send(cmd, len))
					{
						std::cout << "1st string ebmServerStart ebmPipeName is sent !!" << std::endl;
						sprintf(cmd,"forward on");
						len = strlen(cmd);
						if(ebm_pipe.Send(cmd, len))
						{
							std::cout << "2nd string forward on is sent !!" << std::endl;
							floodingOn = true;
							isRelay = CalcRelaySelectionAlgo(host, algo);
							floodingType =SUB_TYPE_SMF_ECDS;
							SendForwardingInfo(isRelay, ebmPipeName);
							std::cout << "SendForwardingInfo() invoked !!" << std::endl;

						}
						else
						{
							std::cout << "sending forward on to pipe failed" << std::endl;

						}
					}
					else
					{
						std::cout << "sending ebmServerStart ebmPipeName to pipe failed" << std::endl;
					}
				}
				else
				{
					std::cout << "Connection to pipe failed" << std::endl;
				}

				}while(connect == false);

			}
			else
			{
				std::cout << "Invalid arguement:: Usage: host algo{ecds,smpr} pipeIdentifier{-ebmClient} pipeName{n1_ebm, n2_ebm etc.}" << std::endl;
			}
	}

////////////////////////////////////////////////////////////////////////////////////







     //EBM server
    std::cout << "Listening for EBM messages" << std::endl;
    EbmServer serv(5514);
    serv.run();
    /*while (1) {
        usleep(1000);
    }*/




    return 0;

} // end of main()


///////////////////////////////////////////////////////////// ////////////////////////////////////////Added by Prateek


bool CalcRelaySelectionAlgo(std::string host, std::string algo)
{

    std::cout << "GETTING NHDP NEIGHBORS FROM " << host << std::endl;
    NhdpProtoClient test_client;
    //NhdpProtoClientNew test_client;

    //get one-hop neighbors
    std::vector<std::string> one_hop_neighbors = test_client.getOneHopNeighbors(host);
    std::cout << "1-hop: ";
    for( std::vector<std::string>::const_iterator i = one_hop_neighbors.begin(); i != one_hop_neighbors.end(); ++i)
    {
        std::cout << *i << ' ';
    }
    std::cout << std::endl;

    //get two-hop neighbors
    std::vector<std::string> two_hop_neighbors = test_client.getTwoHopNeighbors(host);
    std::cout << "2-hop: ";
    for( std::vector<std::string>::const_iterator i = two_hop_neighbors.begin(); i != two_hop_neighbors.end(); ++i)
    {
        std::cout << *i << ' ';
    }
    std::cout << std::endl;

    //E-CDS
    if(algo == "ecds") {
        std::cout << "RUNNING E-CDS" << std::endl;
        ECDS test_ECDS;
        //TODO: get router priority
        int priority = 1;

        //set n0
        std::cout << "setting n0: " << host << std::endl;
        test_ECDS.setn0(priority, host, host);

        //initialize N1 with 1-hop neighbors of n0
        for( std::vector<std::string>::const_iterator i = one_hop_neighbors.begin(); i != one_hop_neighbors.end(); ++i)
        {
            std::cout << "adding to N1: " << *i << std::endl;
            test_ECDS.addN1(priority, *i, *i);
        }
        std::cout << std::endl;

        //initialize N2 with 2-hop neighbors of n0
        for( std::vector<std::string>::const_iterator i = two_hop_neighbors.begin(); i != two_hop_neighbors.end(); ++i)
        {
            std::cout << "adding to N2: " << *i << std::endl;
            test_ECDS.addN2(priority, *i, *i);
        }
        std::cout << std::endl;

        //run algorithm
        std::cout << "running..." << std::endl;
        selected = test_ECDS.run(); //TODO: .run(host)
        std::cout << "n0 selected: " << selected << std::endl;

    }

    //S-MPR
    else if(algo == "smpr") {
        std::cout << "RUNNING S-MPR" << std::endl;
        SMPR test_SMPR;
        //TODO: get router priority
        int priority = 1;

        //set n0
        std::cout << "setting n0: " << host << std::endl;
        test_SMPR.setn0(priority, host, host);

        //initialize MPR to empty

        //initialize N1 with 1-hop neighbors of n0
        for( std::vector<std::string>::const_iterator i = one_hop_neighbors.begin(); i != one_hop_neighbors.end(); ++i)
        {
            std::cout << "adding to N1: " << *i << std::endl;
            test_SMPR.addN1(priority, *i, *i);
        }
        std::cout << std::endl;

        //initialize N2 with 2-hop neighbors of n0
        for( std::vector<std::string>::const_iterator i = two_hop_neighbors.begin(); i != two_hop_neighbors.end(); ++i)
        {
            //std::cout << "[" << *i << "]" << std::endl;
            if((*i).length() > 0)
            {
                //TODO: fix for removing prefix neighbor (e.g. "10.0.0.210.0.0.3")
                std::string i_fix = *i;
                //i_fix = i_fix.erase(0, 8); // remove first ip
                i_fix = i_fix.erase(8, i_fix.length()); // remove second ip

                //TODO: fix
                if(i_fix.length() > 0)
                {
                    //exclude any routers in N1
                    bool in_N1 = false;
                    for( std::vector<std::string>::const_iterator i2 = one_hop_neighbors.begin(); i2 != one_hop_neighbors.end(); ++i2)
                    {
                        if (i_fix.compare(*i2) == 0) {
                            in_N1 = true;
                        }
                    }
                    if(!in_N1) {
                        std::cout << "adding to N2: " << i_fix << "" << std::endl;
                        test_SMPR.addN2(priority, i_fix, i_fix);
                    }
                }
            }
        }
        std::cout << std::endl;

        //run algorithm
        std::cout << "running..." << std::endl;
        std::vector<Router> mpr_set = test_SMPR.run(); //TODO: .run(host)
        std::cout << "mpr_set: ";
        for (unsigned int i = 0; i < mpr_set.size(); i++)
        {
            std::cout << mpr_set[i].address << " ";
        }
        std::cout << std::endl;
    }
    //unknown
    else
    {
        std::cout << "unknown algorithm: " << algo << std::endl;
    }

    return selected;

}




void SendForwardingInfo(bool isRelay, char* ebmPipeName)
{
    std::cout << "SendForwardingInfo()!!!" << std::endl;
    char buffer[512];
    unsigned int len = 0;
    UINT8 floodingType;
    //if(ebm_pipe.IsOpen()) ebm_pipe.Close();
    if(ebm_pipe.Connect(ebmPipeName))
    {
    	std::cout << "Connection to ebmPipe \"%s\" is a success!!!" << ebmPipeName << std::endl;

    if (floodingOn)
    {
        std::cout << "Inside floodingOn!!!" << std::endl;
        switch(floodingType)
        {
            case SUB_TYPE_SMF_SMPR:
            case SUB_TYPE_SMF_CF:
            case SUB_TYPE_SMF_MPRCDS:
            case SUB_TYPE_SMF_ECDS_ETX:
            case SUB_TYPE_SMF_ECDS:

                    if(isRelay)
                    {
                        std::cout << "Inside isRelay , RelayStatus = True!!!" << std::endl;
                        strcpy(buffer,"relay on");
                        len = strlen(buffer);
                        if(ebm_pipe.IsOpen())
                        {
                            std::cout << "Inside isRelay, Inside ebm_pipe.IsOpen() , RelayStatus = True!!!" << std::endl;
                            if(!ebm_pipe.Send(buffer,len))
                            {
                                std::cout << "Inside isRelay, Inside ebm_pipe.IsOpen(),Inside ebm_pipe.Send(buffer,len) , sent failed!!!" << std::endl;
                            }
                            else
                            {
                                std::cout << "Inside isRelay, Inside ebm_pipe.IsOpen(), Inside ebm_pipe.Send(buffer,len) , sent success!!!" << std::endl;

                            }
                        }
                        else
                        {
                            std::cout << "Inside isRelay, Open pipe failed!!!" << std::endl;

                        }
                    }
                    else
                    {
                        std::cout << "Inside not isRelay , RelayStatus = False!!!" << std::endl;
                            strcpy(buffer,"relay off");
                            len = strlen(buffer);
                            if(ebm_pipe.IsOpen())
                            {
								std::cout << "Inside not isRelay, Inside ebm_pipe.IsOpen() , RelayStatus = false!!!" << std::endl;
                                if(!ebm_pipe.Send(buffer,len))
                                {
                                    std::cout << "Inside not isRelay, Inside ebm_pipe.IsOpen(),Inside ebm_pipe.Send(buffer,len) , sent failed!!!" << std::endl;
                                }
                                else
                                {
                                    std::cout << "Inside not isRelay, Inside ebm_pipe.IsOpen(), Inside ebm_pipe.Send(buffer,len) , sent success!!!" << std::endl;
                                }
                            }
                            else
                            {
                                std::cout << "Inside not isRelay, Open pipe failed!!!" << std::endl;
                            }
                    }



                break;
            default:
				std::cout << "SendForwardingInfo ERROR sending smf forwarding info because floodingType %d is not defined\n" << floodingType << std::endl;
                break;
        } //switch
    }
    else
    { //flooding is off so make sure default is off
        if(ebm_pipe.IsOpen())
        {

            strcpy(buffer,"relay off");
            len = strlen(buffer);
            if(!ebm_pipe.Send(buffer,len))
            {
                std::cout << "Inside flooding off, Inside ebm_pipe.IsOpen(),Inside ebm_pipe.Send(buffer,len) , sent failed!!!" << std::endl;
            }
            else
            {
                std::cout << "Inside flooding off, Inside ebm_pipe.IsOpen(),Inside ebm_pipe.Send(buffer,len) , sent success!!!" << std::endl;
            }
        }
        else
        {
            std::cout << "Inside flooding off, Open pipe failed!!!" << std::endl;
        }
    }

    }
    else
    {
    	std::cout << "Connection to ebmPipe \"%s\" failed!!!" << ebmPipeName << std::endl;
    }

}



/////////////////////////////////////////////////////////////////////////////////






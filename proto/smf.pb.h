// Generated by the protocol buffer compiler.  DO NOT EDIT!
// source: smf.proto

#ifndef PROTOBUF_smf_2eproto__INCLUDED
#define PROTOBUF_smf_2eproto__INCLUDED

#include <string>

#include <google/protobuf/stubs/common.h>

#if GOOGLE_PROTOBUF_VERSION < 2006000
#error This file was generated by a newer version of protoc which is
#error incompatible with your Protocol Buffer headers.  Please update
#error your headers.
#endif
#if 2006001 < GOOGLE_PROTOBUF_MIN_PROTOC_VERSION
#error This file was generated by an older version of protoc which is
#error incompatible with your Protocol Buffer headers.  Please
#error regenerate this file with a newer version of protoc.
#endif

#include <google/protobuf/generated_message_util.h>
#include <google/protobuf/message.h>
#include <google/protobuf/repeated_field.h>
#include <google/protobuf/extension_set.h>
#include <google/protobuf/generated_enum_reflection.h>
#include <google/protobuf/unknown_field_set.h>
#include "manet.pb.h"
// @@protoc_insertion_point(includes)

namespace Manet {

// Internal implementation detail -- do not call these.
void  protobuf_AddDesc_smf_2eproto();
void protobuf_AssignDesc_smf_2eproto();
void protobuf_ShutdownFile_smf_2eproto();

class SmfMessageHeader;
class SmfMessage;
class SmfRelayMessage;

enum SmfMessageType {
  RELAY = 1,
  NEIGHBOR_SET = 2,
  TWO_HOP_NEIGHBOR_SET = 3,
  ERROR = 4
};
bool SmfMessageType_IsValid(int value);
const SmfMessageType SmfMessageType_MIN = RELAY;
const SmfMessageType SmfMessageType_MAX = ERROR;
const int SmfMessageType_ARRAYSIZE = SmfMessageType_MAX + 1;

const ::google::protobuf::EnumDescriptor* SmfMessageType_descriptor();
inline const ::std::string& SmfMessageType_Name(SmfMessageType value) {
  return ::google::protobuf::internal::NameOfEnum(
    SmfMessageType_descriptor(), value);
}
inline bool SmfMessageType_Parse(
    const ::std::string& name, SmfMessageType* value) {
  return ::google::protobuf::internal::ParseNamedEnum<SmfMessageType>(
    SmfMessageType_descriptor(), name, value);
}
// ===================================================================

class SmfMessageHeader : public ::google::protobuf::Message {
 public:
  SmfMessageHeader();
  virtual ~SmfMessageHeader();

  SmfMessageHeader(const SmfMessageHeader& from);

  inline SmfMessageHeader& operator=(const SmfMessageHeader& from) {
    CopyFrom(from);
    return *this;
  }

  inline const ::google::protobuf::UnknownFieldSet& unknown_fields() const {
    return _unknown_fields_;
  }

  inline ::google::protobuf::UnknownFieldSet* mutable_unknown_fields() {
    return &_unknown_fields_;
  }

  static const ::google::protobuf::Descriptor* descriptor();
  static const SmfMessageHeader& default_instance();

  void Swap(SmfMessageHeader* other);

  // implements Message ----------------------------------------------

  SmfMessageHeader* New() const;
  void CopyFrom(const ::google::protobuf::Message& from);
  void MergeFrom(const ::google::protobuf::Message& from);
  void CopyFrom(const SmfMessageHeader& from);
  void MergeFrom(const SmfMessageHeader& from);
  void Clear();
  bool IsInitialized() const;

  int ByteSize() const;
  bool MergePartialFromCodedStream(
      ::google::protobuf::io::CodedInputStream* input);
  void SerializeWithCachedSizes(
      ::google::protobuf::io::CodedOutputStream* output) const;
  ::google::protobuf::uint8* SerializeWithCachedSizesToArray(::google::protobuf::uint8* output) const;
  int GetCachedSize() const { return _cached_size_; }
  private:
  void SharedCtor();
  void SharedDtor();
  void SetCachedSize(int size) const;
  public:
  ::google::protobuf::Metadata GetMetadata() const;

  // nested types ----------------------------------------------------

  // accessors -------------------------------------------------------

  // optional .Manet.SmfMessageType messageType = 1;
  inline bool has_messagetype() const;
  inline void clear_messagetype();
  static const int kMessageTypeFieldNumber = 1;
  inline ::Manet::SmfMessageType messagetype() const;
  inline void set_messagetype(::Manet::SmfMessageType value);

  // optional .Manet.ManetOperationalType opType = 2 [default = INFO];
  inline bool has_optype() const;
  inline void clear_optype();
  static const int kOpTypeFieldNumber = 2;
  inline ::Manet::ManetOperationalType optype() const;
  inline void set_optype(::Manet::ManetOperationalType value);

  // optional .Manet.ManetResponseCode responseCode = 3;
  inline bool has_responsecode() const;
  inline void clear_responsecode();
  static const int kResponseCodeFieldNumber = 3;
  inline ::Manet::ManetResponseCode responsecode() const;
  inline void set_responsecode(::Manet::ManetResponseCode value);

  // @@protoc_insertion_point(class_scope:Manet.SmfMessageHeader)
 private:
  inline void set_has_messagetype();
  inline void clear_has_messagetype();
  inline void set_has_optype();
  inline void clear_has_optype();
  inline void set_has_responsecode();
  inline void clear_has_responsecode();

  ::google::protobuf::UnknownFieldSet _unknown_fields_;

  ::google::protobuf::uint32 _has_bits_[1];
  mutable int _cached_size_;
  int messagetype_;
  int optype_;
  int responsecode_;
  friend void  protobuf_AddDesc_smf_2eproto();
  friend void protobuf_AssignDesc_smf_2eproto();
  friend void protobuf_ShutdownFile_smf_2eproto();

  void InitAsDefaultInstance();
  static SmfMessageHeader* default_instance_;
};
// -------------------------------------------------------------------

class SmfMessage : public ::google::protobuf::Message {
 public:
  SmfMessage();
  virtual ~SmfMessage();

  SmfMessage(const SmfMessage& from);

  inline SmfMessage& operator=(const SmfMessage& from) {
    CopyFrom(from);
    return *this;
  }

  inline const ::google::protobuf::UnknownFieldSet& unknown_fields() const {
    return _unknown_fields_;
  }

  inline ::google::protobuf::UnknownFieldSet* mutable_unknown_fields() {
    return &_unknown_fields_;
  }

  static const ::google::protobuf::Descriptor* descriptor();
  static const SmfMessage& default_instance();

  void Swap(SmfMessage* other);

  // implements Message ----------------------------------------------

  SmfMessage* New() const;
  void CopyFrom(const ::google::protobuf::Message& from);
  void MergeFrom(const ::google::protobuf::Message& from);
  void CopyFrom(const SmfMessage& from);
  void MergeFrom(const SmfMessage& from);
  void Clear();
  bool IsInitialized() const;

  int ByteSize() const;
  bool MergePartialFromCodedStream(
      ::google::protobuf::io::CodedInputStream* input);
  void SerializeWithCachedSizes(
      ::google::protobuf::io::CodedOutputStream* output) const;
  ::google::protobuf::uint8* SerializeWithCachedSizesToArray(::google::protobuf::uint8* output) const;
  int GetCachedSize() const { return _cached_size_; }
  private:
  void SharedCtor();
  void SharedDtor();
  void SetCachedSize(int size) const;
  public:
  ::google::protobuf::Metadata GetMetadata() const;

  // nested types ----------------------------------------------------

  // accessors -------------------------------------------------------

  // required .Manet.SmfMessageHeader header = 1;
  inline bool has_header() const;
  inline void clear_header();
  static const int kHeaderFieldNumber = 1;
  inline const ::Manet::SmfMessageHeader& header() const;
  inline ::Manet::SmfMessageHeader* mutable_header();
  inline ::Manet::SmfMessageHeader* release_header();
  inline void set_allocated_header(::Manet::SmfMessageHeader* header);

  // optional bytes message = 2;
  inline bool has_message() const;
  inline void clear_message();
  static const int kMessageFieldNumber = 2;
  inline const ::std::string& message() const;
  inline void set_message(const ::std::string& value);
  inline void set_message(const char* value);
  inline void set_message(const void* value, size_t size);
  inline ::std::string* mutable_message();
  inline ::std::string* release_message();
  inline void set_allocated_message(::std::string* message);

  // @@protoc_insertion_point(class_scope:Manet.SmfMessage)
 private:
  inline void set_has_header();
  inline void clear_has_header();
  inline void set_has_message();
  inline void clear_has_message();

  ::google::protobuf::UnknownFieldSet _unknown_fields_;

  ::google::protobuf::uint32 _has_bits_[1];
  mutable int _cached_size_;
  ::Manet::SmfMessageHeader* header_;
  ::std::string* message_;
  friend void  protobuf_AddDesc_smf_2eproto();
  friend void protobuf_AssignDesc_smf_2eproto();
  friend void protobuf_ShutdownFile_smf_2eproto();

  void InitAsDefaultInstance();
  static SmfMessage* default_instance_;
};
// -------------------------------------------------------------------

class SmfRelayMessage : public ::google::protobuf::Message {
 public:
  SmfRelayMessage();
  virtual ~SmfRelayMessage();

  SmfRelayMessage(const SmfRelayMessage& from);

  inline SmfRelayMessage& operator=(const SmfRelayMessage& from) {
    CopyFrom(from);
    return *this;
  }

  inline const ::google::protobuf::UnknownFieldSet& unknown_fields() const {
    return _unknown_fields_;
  }

  inline ::google::protobuf::UnknownFieldSet* mutable_unknown_fields() {
    return &_unknown_fields_;
  }

  static const ::google::protobuf::Descriptor* descriptor();
  static const SmfRelayMessage& default_instance();

  void Swap(SmfRelayMessage* other);

  // implements Message ----------------------------------------------

  SmfRelayMessage* New() const;
  void CopyFrom(const ::google::protobuf::Message& from);
  void MergeFrom(const ::google::protobuf::Message& from);
  void CopyFrom(const SmfRelayMessage& from);
  void MergeFrom(const SmfRelayMessage& from);
  void Clear();
  bool IsInitialized() const;

  int ByteSize() const;
  bool MergePartialFromCodedStream(
      ::google::protobuf::io::CodedInputStream* input);
  void SerializeWithCachedSizes(
      ::google::protobuf::io::CodedOutputStream* output) const;
  ::google::protobuf::uint8* SerializeWithCachedSizesToArray(::google::protobuf::uint8* output) const;
  int GetCachedSize() const { return _cached_size_; }
  private:
  void SharedCtor();
  void SharedDtor();
  void SetCachedSize(int size) const;
  public:
  ::google::protobuf::Metadata GetMetadata() const;

  // nested types ----------------------------------------------------

  // accessors -------------------------------------------------------

  // required .Manet.SmfMessageHeader header = 1;
  inline bool has_header() const;
  inline void clear_header();
  static const int kHeaderFieldNumber = 1;
  inline const ::Manet::SmfMessageHeader& header() const;
  inline ::Manet::SmfMessageHeader* mutable_header();
  inline ::Manet::SmfMessageHeader* release_header();
  inline void set_allocated_header(::Manet::SmfMessageHeader* header);

  // required uint32 routerId = 2;
  inline bool has_routerid() const;
  inline void clear_routerid();
  static const int kRouterIdFieldNumber = 2;
  inline ::google::protobuf::uint32 routerid() const;
  inline void set_routerid(::google::protobuf::uint32 value);

  // required bool isRelay = 3;
  inline bool has_isrelay() const;
  inline void clear_isrelay();
  static const int kIsRelayFieldNumber = 3;
  inline bool isrelay() const;
  inline void set_isrelay(bool value);

  // @@protoc_insertion_point(class_scope:Manet.SmfRelayMessage)
 private:
  inline void set_has_header();
  inline void clear_has_header();
  inline void set_has_routerid();
  inline void clear_has_routerid();
  inline void set_has_isrelay();
  inline void clear_has_isrelay();

  ::google::protobuf::UnknownFieldSet _unknown_fields_;

  ::google::protobuf::uint32 _has_bits_[1];
  mutable int _cached_size_;
  ::Manet::SmfMessageHeader* header_;
  ::google::protobuf::uint32 routerid_;
  bool isrelay_;
  friend void  protobuf_AddDesc_smf_2eproto();
  friend void protobuf_AssignDesc_smf_2eproto();
  friend void protobuf_ShutdownFile_smf_2eproto();

  void InitAsDefaultInstance();
  static SmfRelayMessage* default_instance_;
};
// ===================================================================


// ===================================================================

// SmfMessageHeader

// optional .Manet.SmfMessageType messageType = 1;
inline bool SmfMessageHeader::has_messagetype() const {
  return (_has_bits_[0] & 0x00000001u) != 0;
}
inline void SmfMessageHeader::set_has_messagetype() {
  _has_bits_[0] |= 0x00000001u;
}
inline void SmfMessageHeader::clear_has_messagetype() {
  _has_bits_[0] &= ~0x00000001u;
}
inline void SmfMessageHeader::clear_messagetype() {
  messagetype_ = 1;
  clear_has_messagetype();
}
inline ::Manet::SmfMessageType SmfMessageHeader::messagetype() const {
  // @@protoc_insertion_point(field_get:Manet.SmfMessageHeader.messageType)
  return static_cast< ::Manet::SmfMessageType >(messagetype_);
}
inline void SmfMessageHeader::set_messagetype(::Manet::SmfMessageType value) {
  assert(::Manet::SmfMessageType_IsValid(value));
  set_has_messagetype();
  messagetype_ = value;
  // @@protoc_insertion_point(field_set:Manet.SmfMessageHeader.messageType)
}

// optional .Manet.ManetOperationalType opType = 2 [default = INFO];
inline bool SmfMessageHeader::has_optype() const {
  return (_has_bits_[0] & 0x00000002u) != 0;
}
inline void SmfMessageHeader::set_has_optype() {
  _has_bits_[0] |= 0x00000002u;
}
inline void SmfMessageHeader::clear_has_optype() {
  _has_bits_[0] &= ~0x00000002u;
}
inline void SmfMessageHeader::clear_optype() {
  optype_ = 9;
  clear_has_optype();
}
inline ::Manet::ManetOperationalType SmfMessageHeader::optype() const {
  // @@protoc_insertion_point(field_get:Manet.SmfMessageHeader.opType)
  return static_cast< ::Manet::ManetOperationalType >(optype_);
}
inline void SmfMessageHeader::set_optype(::Manet::ManetOperationalType value) {
  assert(::Manet::ManetOperationalType_IsValid(value));
  set_has_optype();
  optype_ = value;
  // @@protoc_insertion_point(field_set:Manet.SmfMessageHeader.opType)
}

// optional .Manet.ManetResponseCode responseCode = 3;
inline bool SmfMessageHeader::has_responsecode() const {
  return (_has_bits_[0] & 0x00000004u) != 0;
}
inline void SmfMessageHeader::set_has_responsecode() {
  _has_bits_[0] |= 0x00000004u;
}
inline void SmfMessageHeader::clear_has_responsecode() {
  _has_bits_[0] &= ~0x00000004u;
}
inline void SmfMessageHeader::clear_responsecode() {
  responsecode_ = 1;
  clear_has_responsecode();
}
inline ::Manet::ManetResponseCode SmfMessageHeader::responsecode() const {
  // @@protoc_insertion_point(field_get:Manet.SmfMessageHeader.responseCode)
  return static_cast< ::Manet::ManetResponseCode >(responsecode_);
}
inline void SmfMessageHeader::set_responsecode(::Manet::ManetResponseCode value) {
  assert(::Manet::ManetResponseCode_IsValid(value));
  set_has_responsecode();
  responsecode_ = value;
  // @@protoc_insertion_point(field_set:Manet.SmfMessageHeader.responseCode)
}

// -------------------------------------------------------------------

// SmfMessage

// required .Manet.SmfMessageHeader header = 1;
inline bool SmfMessage::has_header() const {
  return (_has_bits_[0] & 0x00000001u) != 0;
}
inline void SmfMessage::set_has_header() {
  _has_bits_[0] |= 0x00000001u;
}
inline void SmfMessage::clear_has_header() {
  _has_bits_[0] &= ~0x00000001u;
}
inline void SmfMessage::clear_header() {
  if (header_ != NULL) header_->::Manet::SmfMessageHeader::Clear();
  clear_has_header();
}
inline const ::Manet::SmfMessageHeader& SmfMessage::header() const {
  // @@protoc_insertion_point(field_get:Manet.SmfMessage.header)
  return header_ != NULL ? *header_ : *default_instance_->header_;
}
inline ::Manet::SmfMessageHeader* SmfMessage::mutable_header() {
  set_has_header();
  if (header_ == NULL) header_ = new ::Manet::SmfMessageHeader;
  // @@protoc_insertion_point(field_mutable:Manet.SmfMessage.header)
  return header_;
}
inline ::Manet::SmfMessageHeader* SmfMessage::release_header() {
  clear_has_header();
  ::Manet::SmfMessageHeader* temp = header_;
  header_ = NULL;
  return temp;
}
inline void SmfMessage::set_allocated_header(::Manet::SmfMessageHeader* header) {
  delete header_;
  header_ = header;
  if (header) {
    set_has_header();
  } else {
    clear_has_header();
  }
  // @@protoc_insertion_point(field_set_allocated:Manet.SmfMessage.header)
}

// optional bytes message = 2;
inline bool SmfMessage::has_message() const {
  return (_has_bits_[0] & 0x00000002u) != 0;
}
inline void SmfMessage::set_has_message() {
  _has_bits_[0] |= 0x00000002u;
}
inline void SmfMessage::clear_has_message() {
  _has_bits_[0] &= ~0x00000002u;
}
inline void SmfMessage::clear_message() {
  if (message_ != &::google::protobuf::internal::GetEmptyStringAlreadyInited()) {
    message_->clear();
  }
  clear_has_message();
}
inline const ::std::string& SmfMessage::message() const {
  // @@protoc_insertion_point(field_get:Manet.SmfMessage.message)
  return *message_;
}
inline void SmfMessage::set_message(const ::std::string& value) {
  set_has_message();
  if (message_ == &::google::protobuf::internal::GetEmptyStringAlreadyInited()) {
    message_ = new ::std::string;
  }
  message_->assign(value);
  // @@protoc_insertion_point(field_set:Manet.SmfMessage.message)
}
inline void SmfMessage::set_message(const char* value) {
  set_has_message();
  if (message_ == &::google::protobuf::internal::GetEmptyStringAlreadyInited()) {
    message_ = new ::std::string;
  }
  message_->assign(value);
  // @@protoc_insertion_point(field_set_char:Manet.SmfMessage.message)
}
inline void SmfMessage::set_message(const void* value, size_t size) {
  set_has_message();
  if (message_ == &::google::protobuf::internal::GetEmptyStringAlreadyInited()) {
    message_ = new ::std::string;
  }
  message_->assign(reinterpret_cast<const char*>(value), size);
  // @@protoc_insertion_point(field_set_pointer:Manet.SmfMessage.message)
}
inline ::std::string* SmfMessage::mutable_message() {
  set_has_message();
  if (message_ == &::google::protobuf::internal::GetEmptyStringAlreadyInited()) {
    message_ = new ::std::string;
  }
  // @@protoc_insertion_point(field_mutable:Manet.SmfMessage.message)
  return message_;
}
inline ::std::string* SmfMessage::release_message() {
  clear_has_message();
  if (message_ == &::google::protobuf::internal::GetEmptyStringAlreadyInited()) {
    return NULL;
  } else {
    ::std::string* temp = message_;
    message_ = const_cast< ::std::string*>(&::google::protobuf::internal::GetEmptyStringAlreadyInited());
    return temp;
  }
}
inline void SmfMessage::set_allocated_message(::std::string* message) {
  if (message_ != &::google::protobuf::internal::GetEmptyStringAlreadyInited()) {
    delete message_;
  }
  if (message) {
    set_has_message();
    message_ = message;
  } else {
    clear_has_message();
    message_ = const_cast< ::std::string*>(&::google::protobuf::internal::GetEmptyStringAlreadyInited());
  }
  // @@protoc_insertion_point(field_set_allocated:Manet.SmfMessage.message)
}

// -------------------------------------------------------------------

// SmfRelayMessage

// required .Manet.SmfMessageHeader header = 1;
inline bool SmfRelayMessage::has_header() const {
  return (_has_bits_[0] & 0x00000001u) != 0;
}
inline void SmfRelayMessage::set_has_header() {
  _has_bits_[0] |= 0x00000001u;
}
inline void SmfRelayMessage::clear_has_header() {
  _has_bits_[0] &= ~0x00000001u;
}
inline void SmfRelayMessage::clear_header() {
  if (header_ != NULL) header_->::Manet::SmfMessageHeader::Clear();
  clear_has_header();
}
inline const ::Manet::SmfMessageHeader& SmfRelayMessage::header() const {
  // @@protoc_insertion_point(field_get:Manet.SmfRelayMessage.header)
  return header_ != NULL ? *header_ : *default_instance_->header_;
}
inline ::Manet::SmfMessageHeader* SmfRelayMessage::mutable_header() {
  set_has_header();
  if (header_ == NULL) header_ = new ::Manet::SmfMessageHeader;
  // @@protoc_insertion_point(field_mutable:Manet.SmfRelayMessage.header)
  return header_;
}
inline ::Manet::SmfMessageHeader* SmfRelayMessage::release_header() {
  clear_has_header();
  ::Manet::SmfMessageHeader* temp = header_;
  header_ = NULL;
  return temp;
}
inline void SmfRelayMessage::set_allocated_header(::Manet::SmfMessageHeader* header) {
  delete header_;
  header_ = header;
  if (header) {
    set_has_header();
  } else {
    clear_has_header();
  }
  // @@protoc_insertion_point(field_set_allocated:Manet.SmfRelayMessage.header)
}

// required uint32 routerId = 2;
inline bool SmfRelayMessage::has_routerid() const {
  return (_has_bits_[0] & 0x00000002u) != 0;
}
inline void SmfRelayMessage::set_has_routerid() {
  _has_bits_[0] |= 0x00000002u;
}
inline void SmfRelayMessage::clear_has_routerid() {
  _has_bits_[0] &= ~0x00000002u;
}
inline void SmfRelayMessage::clear_routerid() {
  routerid_ = 0u;
  clear_has_routerid();
}
inline ::google::protobuf::uint32 SmfRelayMessage::routerid() const {
  // @@protoc_insertion_point(field_get:Manet.SmfRelayMessage.routerId)
  return routerid_;
}
inline void SmfRelayMessage::set_routerid(::google::protobuf::uint32 value) {
  set_has_routerid();
  routerid_ = value;
  // @@protoc_insertion_point(field_set:Manet.SmfRelayMessage.routerId)
}

// required bool isRelay = 3;
inline bool SmfRelayMessage::has_isrelay() const {
  return (_has_bits_[0] & 0x00000004u) != 0;
}
inline void SmfRelayMessage::set_has_isrelay() {
  _has_bits_[0] |= 0x00000004u;
}
inline void SmfRelayMessage::clear_has_isrelay() {
  _has_bits_[0] &= ~0x00000004u;
}
inline void SmfRelayMessage::clear_isrelay() {
  isrelay_ = false;
  clear_has_isrelay();
}
inline bool SmfRelayMessage::isrelay() const {
  // @@protoc_insertion_point(field_get:Manet.SmfRelayMessage.isRelay)
  return isrelay_;
}
inline void SmfRelayMessage::set_isrelay(bool value) {
  set_has_isrelay();
  isrelay_ = value;
  // @@protoc_insertion_point(field_set:Manet.SmfRelayMessage.isRelay)
}


// @@protoc_insertion_point(namespace_scope)

}  // namespace Manet

#ifndef SWIG
namespace google {
namespace protobuf {

template <> struct is_proto_enum< ::Manet::SmfMessageType> : ::google::protobuf::internal::true_type {};
template <>
inline const EnumDescriptor* GetEnumDescriptor< ::Manet::SmfMessageType>() {
  return ::Manet::SmfMessageType_descriptor();
}

}  // namespace google
}  // namespace protobuf
#endif  // SWIG

// @@protoc_insertion_point(global_scope)

#endif  // PROTOBUF_smf_2eproto__INCLUDED

#!/bin/bash
cd "$(dirname "$0")"
echo "building protocol buffers..."
protos=( "manet" "olsr" "nhdp" "ebm" "smf" )
for proto in "${protos[@]}"
do
    echo $proto
    protoc --cpp_out=./ $proto.proto
done


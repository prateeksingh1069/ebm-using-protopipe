OBJS = proto/manet.pb.o proto/nhdp.pb.o proto/ebm.pb.o proto/smf.pb.o client/NhdpProtoClient.o algo/ECDS.o algo/SMPR.o client/EbmProtoClient.o server/EbmServer.o
#CC = LD_LIBRARY_PATH=/home/quest/Downloads/poco-1.6.0/lib/Linux/x86_64 g++
CC = g++
DEBUG = -g
SYSTEM_HAVES = -DLINUX -DHAVE_IPV6 -DHAVE_GETLOGIN -D_FILE_OFFSET_BITS=64 -DHAVE_LOCKF \
-DHAVE_OLD_SIGNALHANDLER -DHAVE_DIRFD -DHAVE_ASSERT -DNO_SCM_RIGHTS -DHAVE_SCHED
CFLAGS = -std=c++0x -Wall -c $(DEBUG) -g -DMNE_SUPPORT -DPROTO_DEBUG -DUNIX -D_FILE_OFFSET_BITS=64 -O -fPIC $(SYSTEM_HAVES)
PROTOLIB = protolib
LFLAGS = -std=c++0x -Wl,-t,--no-as-needed -Wall $(DEBUG) -lprotobuf -lPocoFoundation -lPocoNet -lprotolib `pkg-config --cflags --libs protobuf`
LIBPROTO = $(PROTOLIB)/lib/libprotokit.a

all : Ebm tests

tests : NhdpClientTest NhdpServerTest EbmClientTest EbmServerTest UDPReactorTest NhdpClientOneHopTest NhdpClientTwoHopTest New_NHDP_client_oneHop New_NHDP_client_twoHop

Ebm : $(OBJS)
	$(CC) Ebm.cpp $(OBJS) -o Ebm $(LFLAGS)

manet.pb.o : proto/manet.pb.cc
	$(CC) -c proto/manet.pb.cc -o proto/manet.pb.o $(LFLAGS)

nhdp.pb.o : proto/nhdp.pb.cc
	$(CC) -c proto/nhdp.pb.cc -o proto/nhdp.pb.o $(LFLAGS)

ebm.pb.o : proto/ebm.pb.cc
	$(CC) -c proto/ebm.pb.cc -o proto/ebm.pb.o $(LFLAGS)

smf.pb.o : proto/smf.pb.cc
	$(CC) -c proto/smf.pb.cc -o proto/smf.pb.o $(LFLAGS)

NhdpProtoClient.o : client/NhdpProtoClient.cpp
	$(CC) -c client/NhdpProtoClient.cpp -o client/NhdpProtoClient.o $(LFLAGS)

ECDS.o : algo/ECDS.cpp
	$(CC) -c algo/ECDS.cpp -o algo/ECDS.o $(LFLAGS)

SMPR.o : algo/SMPR.cpp
	$(CC) -c algo/SMPR.cpp -o algo/SMPR.o $(LFLAGS)

EbmProtoClient.o : client/EbmProtoClient.cpp
	$(CC) -c client/EbmProtoClient.cpp -o client/EbmProtoClient.o $(LFLAGS)

NhdpClientTest : test/NhdpClientTest.cc
	$(CC) test/NhdpClientTest.cc proto/manet.pb.o proto/nhdp.pb.o -o test/NhdpClientTest $(LFLAGS)

NhdpClientOneHopTest : test/NhdpClientOneHopTest.cc
	$(CC) test/NhdpClientOneHopTest.cc proto/manet.pb.o proto/nhdp.pb.o -o test/NhdpClientOneHopTest $(LFLAGS)

NhdpClientTwoHopTest : test/NhdpClientTwoHopTest.cc
	$(CC) test/NhdpClientTwoHopTest.cc proto/manet.pb.o proto/nhdp.pb.o -o test/NhdpClientTwoHopTest $(LFLAGS)

New_NHDP_client_oneHop : test/New_NHDP_client_oneHop.cc
	$(CC) test/New_NHDP_client_oneHop.cc proto/manet.pb.o proto/nhdp.pb.o -o test/New_NHDP_client_oneHop $(LFLAGS)

New_NHDP_client_twoHop : test/New_NHDP_client_twoHop.cc
	$(CC) test/New_NHDP_client_twoHop.cc proto/manet.pb.o proto/nhdp.pb.o -o test/New_NHDP_client_twoHop $(LFLAGS)

NhdpServerTest : test/NhdpServerTest.cc
	$(CC) test/NhdpServerTest.cc proto/manet.pb.o proto/nhdp.pb.o -o test/NhdpServerTest $(LFLAGS)

EbmClientTest : test/EbmClientTest.cc
	$(CC) test/EbmClientTest.cc proto/manet.pb.o proto/ebm.pb.o -o test/EbmClientTest $(LFLAGS)

EbmServerTest : test/EbmServerTest.cc
	$(CC) test/EbmServerTest.cc proto/manet.pb.o proto/ebm.pb.o -o test/EbmServerTest $(LFLAGS)

UDPReactorTest : test/UDPReactorTest.cpp
	$(CC) test/UDPReactorTest.cpp -o test/UDPReactorTest -lPocoFoundation -lPocoNet

EbmServer.o : server/EbmServer.cpp
	$(CC) -c server/EbmServer.cpp -o server/EbmServer.o $(LFLAGS)

clean:
	\rm Ebm algo/*.o client/*.o proto/*.o server/*.o test/NhdpClientOneHopTest.o test/NhdpClientTwoHopTest.o test/NhdpClientTwoHopTest test/NhdpClientOneHopTest test/New_NHDP_client_oneHop test/New_NHDP_client_twoHop test/NhdpClientTest test/NhdpServerTest test/EbmClientTest test/EbmServerTest test/UDPReactorTest

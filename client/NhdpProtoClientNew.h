// NhdpProtoClientNew.h
//
#ifndef NHDPPROTOCLIENTNEW_H
#define NHDPPROTOCLIENTNEW_H

#include <vector>
#include <netinet/in.h>
#include "../proto/nhdp.pb.h"
#include "../protolib/include/protoAddress.h"
#include "../protolib/include/protoSocket.h"

struct Router {
    int priority;
    std::string id;
    std::string address;
    bool visited;
};

class NhdpProtoClientNew
{
public:
    NhdpProtoClientNew();
    ~NhdpProtoClientNew();
        
    std::vector<std::string> getOneHopNeighbors(std::string host);
    std::vector<std::string> getTwoHopNeighbors(std::string host);
private:
    static const int NHDP_PORT = 5555;
	
	char buf[8192];

};
#endif

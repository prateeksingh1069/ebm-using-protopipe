// EbmProtoClient.h
//
#ifndef NHDPPROTOCLIENT_H
#define NHDPPROTOCLIENT_H

#include <vector>
#include <netinet/in.h>
#include "../proto/ebm.pb.h"

struct EbmRouter {
    int priority;
    std::string id;
    std::string address;
    bool isRelay;
};

class EbmProtoClient
{
public:
    EbmProtoClient();
    ~EbmProtoClient();
        
    bool getIsRelay(std::string host);
private:
    static const bool DEBUG_ENABLED = false;
    static const int EBM_PORT = 5556;

    void openConnection(std::string host);
    void closeConnection();

    Manet::EbmMessage createMessage(Manet::ManetOperationalType optype, Manet::EbmMessageType messagetype);
    void printMessage(Manet::EbmMessage message);
    void printRelayMessage(Manet::EbmRelayMessage message);
    std::string serializeMessage(Manet::EbmMessage message);
    std::string serializeRelayMessage(Manet::EbmRelayMessage message);
    Manet::EbmMessage parseMessage(std::string serial);
    Manet::EbmRelayMessage parseRelayMessage(std::string serial);
    bool sendMessage(Manet::EbmMessage message);
    bool receiveRelayMessage(Manet::EbmRelayMessage* message);
    
    bool connected;
    int sock, n;
    unsigned int length;
    struct sockaddr_in server, from;
    struct hostent *hp;
    char buffer[256];
    
    //Manet::EbmMessage ebm_message, ebm_response;
    //std::string ebm_message_serial;
    //std::vector<std::string> one_hop_neighbors, two_hop_neighbors;
};
#endif

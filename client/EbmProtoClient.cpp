// EbmProtoClient.cpp
//
#include "EbmProtoClient.h"
#include <sys/types.h>
#include <sys/socket.h>
#include <netinet/in.h>
#include <arpa/inet.h>
#include <netdb.h>
#include <stdio.h>
#include <stdlib.h>
#include <unistd.h>
#include <string.h>
#include <iostream>
#include <fstream>
#include <string>
#include <sstream>
#include "../proto/ebm.pb.h"

EbmProtoClient::EbmProtoClient()
{
    connected = false;
}

EbmProtoClient::~EbmProtoClient()
{
    closeConnection();
}

bool EbmProtoClient::getIsRelay(std::string host)
{
    bool isRelay;
    openConnection(host);
    
    Manet::EbmMessage ebm_message = createMessage(Manet::GET, Manet::RELAY);
    ebm_message.set_message(host);

    if (sendMessage(ebm_message))
    {
        Manet::EbmRelayMessage ebm_response;
        if (receiveRelayMessage(&ebm_response))
        {
            isRelay = ebm_response.isrelay();
        }
    }
    closeConnection();
    return isRelay;
}

void EbmProtoClient::openConnection(std::string host)
{
    if (!connected)
    {
        sock= socket(AF_INET, SOCK_DGRAM, 0);
        if (sock < 0)
        {
            std::cout << "ERROR: could not create socket" << std::endl;
            return;
        }
        server.sin_family = AF_INET;
        hp = gethostbyname(host.c_str());
        if (hp == 0) 
        {
            std::cout << "ERROR: unknown host " + host << std::endl;
            return;
        }

        bcopy((char *)hp->h_addr, (char *)&server.sin_addr, hp->h_length);
        server.sin_port = htons(EBM_PORT);
        length=sizeof(struct sockaddr_in);
        
        connected = true;
    }
}

void EbmProtoClient::closeConnection()
{
    if (connected)
    {
        close(sock);
        connected = false;
    }
}

Manet::EbmMessage EbmProtoClient::createMessage(Manet::ManetOperationalType optype, Manet::EbmMessageType messagetype)
{
    Manet::EbmMessage message;
    message.mutable_header()->set_optype(optype);
    message.mutable_header()->set_messagetype(messagetype);
    //message.mutable_header()->set_responsecode(Manet::SUCCESSFUL);
    //message.set_message("");
    return message;
}

void EbmProtoClient::printMessage(Manet::EbmMessage message)
{
    std::cout << "[ ";
    if (message.has_header())
    {
        std::cout << Manet::ManetOperationalType_Name(message.header().optype());
        std::cout << " | " << Manet::EbmMessageType_Name(message.header().messagetype());
        std::cout << " | " << Manet::ManetResponseCode_Name(message.header().responsecode());
    }
    std::cout << " ] { ";
    if (message.has_message())
    {
        std::cout << message.message();
    }
    std::cout << " }" << std::endl;
}

void EbmProtoClient::printRelayMessage(Manet::EbmRelayMessage message)
{
    std::cout << "[ ";
    if (message.has_header())
    {
        std::cout << Manet::ManetOperationalType_Name(message.header().optype());
        std::cout << " | " << Manet::EbmMessageType_Name(message.header().messagetype());
        std::cout << " | " << Manet::ManetResponseCode_Name(message.header().responsecode());
    }
    std::cout << " ] { ";
    if (message.has_isrelay())
    {
        std::cout << message.isrelay();
    }
    std::cout << " }" << std::endl;
}

std::string EbmProtoClient::serializeMessage(Manet::EbmMessage message)
{
    std::string serial;
    message.SerializeToString(&serial);
    return serial;
}

std::string EbmProtoClient::serializeRelayMessage(Manet::EbmRelayMessage message)
{
    std::string serial;
    message.SerializeToString(&serial);
    return serial;
}

Manet::EbmMessage EbmProtoClient::parseMessage(std::string serial)
{
    Manet::EbmMessage message;
    message.ParseFromString(serial);
    return message;
}

Manet::EbmRelayMessage EbmProtoClient::parseRelayMessage(std::string serial)
{
    Manet::EbmRelayMessage message;
    message.ParseFromString(serial);
    return message;
}

bool EbmProtoClient::sendMessage(Manet::EbmMessage message)
{
    if (DEBUG_ENABLED)
    {
        std::cout << "sending: ";
        printMessage(message);
    }
    
    std::string ebm_message_serial = serializeMessage(message);

    n=sendto(sock,ebm_message_serial.data(),ebm_message_serial.length(),0,(const struct sockaddr *)&server,length);
    if (n < 0) 
    {
        std::cout << "ERROR: could not send" << std::endl;
        return false;
    }
    return true;
}

bool EbmProtoClient::receiveRelayMessage(Manet::EbmRelayMessage* message)
{
    n = recvfrom(sock,buffer,256,0,(struct sockaddr *)&from, &length);
    if (n < 0)
    {
        std::cout << "ERROR: could not receive" << std::endl;
        return false;
    }
    
    std::string ebm_response_serial (buffer);
    *message = parseRelayMessage(ebm_response_serial);
    
    if (DEBUG_ENABLED)
    {
        std::cout << "received: ";
        printRelayMessage(*message);
    }
    
    if ((*message).header().responsecode() != Manet::SUCCESSFUL)
    {
        std::cout << "WARNING: received response code " << Manet::ManetResponseCode_Name((*message).header().responsecode()) << std::endl;
        return false;
    }
    return true;
}


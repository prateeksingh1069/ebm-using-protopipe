// NhdpProtoClient.cpp
//
#include <sys/types.h>
#include <sys/socket.h>
#include <netinet/in.h>
#include <arpa/inet.h>
#include "NhdpProtoClient.h"
#include <netdb.h>
#include <stdio.h>
#include <stdlib.h>
#include <unistd.h>
#include <string.h>
#include <iostream>
#include <fstream>
#include <string>
#include <sstream>
#include <vector>
//#include "../../protolib/include/protoAddress.h"
//#include "../../protolib/include/protoSocket.h"
#include "../proto/nhdp.pb.h"
#include "Poco/Net/DatagramSocket.h"
#include "Poco/Net/SocketAddress.h"


using Poco::Net::DatagramSocket;
using Poco::Net::SocketAddress;
using Poco::UInt16;

NhdpProtoClient::NhdpProtoClient()
{

}

NhdpProtoClient::~NhdpProtoClient()
{
	//ProtoSocket socket(ProtoSocket::UDP);
    //socket.Close();
	DatagramSocket socket;
	socket.close();
}

std::vector<std::string> NhdpProtoClient::getOneHopNeighbors(std::string host)
{
    std::vector<std::string> one_hop_neighbors;
    //openConnection(host);
	unsigned int size = 8192;
    Poco::UInt16 NHDP_PORT1 = 5555;
    //ProtoAddress addr;
	//ProtoSocket socket(ProtoSocket::UDP);

    //addr.ResolveFromString(host.c_str());
    //addr.SetPort(NHDP_PORT);

	SocketAddress addr(host, NHDP_PORT1);
	SocketAddress sa;

	DatagramSocket socket;

    Manet::NeighborSetMessage neighborMessage;

    Manet::NhdpMessage request;
    Manet::NhdpMessageHeader *header = request.mutable_header();
    header->set_messagetype(Manet::NEIGHBOR_SET);
    header->set_optype(Manet::GET);
    request.set_message(neighborMessage.SerializeAsString());

    if(socket.sendTo(request.SerializeAsString().c_str(), request.ByteSize(), addr))
    {
        std::cout << "successful send" << std::endl;

        if(socket.receiveFrom(buf,size,sa))
        {
            Manet::NhdpMessage response;
            response.ParseFromArray(buf,size);
            neighborMessage.ParseFromString(response.message());

            for( int i = 0; i < neighborMessage.neighbors_size(); ++i)
            {
				//test
                std::cout << neighborMessage.neighbors(i).addresses(0).address() << " symetric: ";
                if(neighborMessage.neighbors(i).has_status() && neighborMessage.neighbors(i).status() == Manet::NeighborMessage_NeighborStatus_SYMMETRIC)
                    std::cout << "true";
                else
                    std::cout << "false";
                std::cout << std::endl;

				std::string neighbor;
				neighbor = neighborMessage.neighbors(i).addresses(0).address();
				one_hop_neighbors.push_back(neighbor);
            }
        }
    }
    else
    {
        std::cout << "send to socket failed" << std::endl;
    }

	socket.close();

    //closeConnection();
    return one_hop_neighbors;
}

std::vector<std::string> NhdpProtoClient::getTwoHopNeighbors(std::string host)
{
    std::vector<std::string> two_hop_neighbors;
    //openConnection(host);

	unsigned int size = 8192;
    Poco::UInt16 NHDP_PORT1 = 5555;
    char buf1[8192];
    //ProtoAddress addr;
	//ProtoSocket socket(ProtoSocket::UDP);

    //addr.ResolveFromString(host.c_str());
    //addr.SetPort(NHDP_PORT);

	SocketAddress addr(host, NHDP_PORT1);
	SocketAddress sa;

	DatagramSocket socket;

	Manet::TwoHopNeighborSetMessage neighborMessage2;

    Manet::NhdpMessage request2;
    Manet::NhdpMessageHeader *header2 = request2.mutable_header();
    header2->set_messagetype(Manet::TWO_HOP_NEIGHBOR_SET);
    header2->set_optype(Manet::GET);
    request2.set_message(neighborMessage2.SerializeAsString());

    if(socket.sendTo(request2.SerializeAsString().c_str(), request2.ByteSize(), addr))
    {
        std::cout << "successful send" << std::endl;

        if(socket.receiveFrom(buf1,size,sa))
        {
            std::cout << "successfully recieved from NHDP" << std::endl;
            //std::cout << "socket.receiveFrom(buf,size,sa) : " << socket.receiveFrom(buf,size,sa) << std::endl;
            Manet::NhdpMessage response2;
            response2.ParseFromArray(buf1,size);
            neighborMessage2.ParseFromString(response2.message());
            std::cout << "neighborMessage2.neighbors_size() : " << neighborMessage2.neighbors_size() << std::endl;

            if(0 == neighborMessage2.neighbors_size())
            {
                std::cout << "Zero neighbors." << std::endl;
            }

            for( int i = 0; i < neighborMessage2.neighbors_size(); ++i)
            {
				if( neighborMessage2.neighbors(i).has_twohopaddress() )
				two_hop_neighbors.push_back(neighborMessage2.neighbors(i).twohopaddress());

				//test
                for( int j = 0; j < neighborMessage2.neighbors(i).onehopneighboraddress_size(); ++j)
                {
                    std::cout << "One Hop Addrs:" << std::endl;
                    std::cout << neighborMessage2.neighbors(i).onehopneighboraddress(j) << std::endl;
                }
                if( neighborMessage2.neighbors(i).has_twohopaddress() )
                    std::cout << "Two Hop Addr: " << neighborMessage2.neighbors(i).twohopaddress() << std::endl;
                if( neighborMessage2.neighbors(i).has_expiration() )
                    std::cout << "Expires: " << neighborMessage2.neighbors(i).expiration();

                std::cout << std::endl << std::endl;
            }
        }
    }
    else
    {
        std::cout << "send to socket failed" << std::endl;
    }

	socket.close();

	//closeConnection();
    return two_hop_neighbors;
}


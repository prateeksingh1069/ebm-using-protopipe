// NhdpProtoClient.h
//
#ifndef NHDPPROTOCLIENT_H
#define NHDPPROTOCLIENT_H

#include <vector>
#include <netinet/in.h>
#include <sys/types.h>
#include <sys/socket.h>
#include <arpa/inet.h>
#include "../proto/nhdp.pb.h"
//#include "../../protolib/include/protoAddress.h"
//#include "../../protolib/include/protoSocket.h"
#include "Poco/Net/DatagramSocket.h"
#include "Poco/Net/SocketAddress.h"

struct Router {
    int priority;
    std::string id;
    std::string address;
    bool visited;
};

class NhdpProtoClient
{
public:
    NhdpProtoClient();
    ~NhdpProtoClient();
        
    std::vector<std::string> getOneHopNeighbors(std::string host);
    std::vector<std::string> getTwoHopNeighbors(std::string host);
private:
    static const int NHDP_PORT = 5555;
	
	char buf[8192];

};
#endif
